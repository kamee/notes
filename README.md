# notes

այս պահոցում/շտեմարանում հաւաքելու եմ տարբեր յղումներ եւ նոթեր, որոնք կուտակւում են ուսումնասիրելու/սովորելու ընթացքում եւ որոնք երեւի օգտակար կարող են լինել այլոց։

***

### language design

[In Go, pointers (mostly) don't go with slices in practice. HN](https://news.ycombinator.com/item?id=28344938)

[Why C has Pointers by Andrew Hardwick](http://duramecho.com/ComputerInformation/WhyCPointers.html)

[Energy efficiency of programming languages by Marian's Garden](https://www.sendung.de/2022-07-24/programming-languages-energy-efficiency/)

[Less is exponentially more  by rob pike](https://commandcenter.blogspot.com/2012/06/less-is-exponentially-more.html)

[Dynamic Languages are Static Languages by Robert Harper](https://existentialtype.wordpress.com/2011/03/19/dynamic-languages-are-static-languages/)

### networking

(HTTP Signatures. draft-cavage-http-signatures-12)[https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12]

### concurrency

[Blocking I/O, Nonblocking I/O, And Epoll | eklitzke.org](https://eklitzke.org/blocking-io-nonblocking-io-and-epoll)

(Re: proc fs and shared pids: There is NO reason to think that "threads" and "processes" are separate
entities. Linus)[https://lkml.iu.edu/hypermail/linux/kernel/9608/0191.html]

[Mutex vs. Semaphores – Part 1: Semaphores by Niall Cooling](https://blog.feabhas.com/2009/09/mutex-vs-semaphores-%e2%80%93-part-1-semaphores/)

[Mutex vs. Semaphores – Part 3 (final part): Mutual Exclusion Problems by Niall Cooling](https://blog.feabhas.com/2009/10/mutex-vs-semaphores-%E2%80%93-part-3-final-part-mutual-exclusion-problems/)

(1024cores. Dmitry Vyukov blog)[https://www.1024cores.net/home]

(Mutex vs Atomic. CoffeeBeforeArch)[https://coffeebeforearch.github.io/2020/08/04/atomic-vs-mutex.html]

(Comparing the performance of atomic, spinlock and mutex)[https://demin.ws/blog/english/2012/05/05/atomic-spinlock-mutex/]

(You Can Do Any Kind of Atomic Read-Modify-Write Operation. Jeff Preshing)[https://preshing.com/20150402/you-can-do-any-kind-of-atomic-read-modify-write-operation/]

(The Trouble With Locks. C/C++ Users Journal, 23(3), March 2005.)[http://gotw.ca/publications/mill36.htm]

(The difference between mutex and rwmutex)[http://programmer.help/blogs/difference-between-mutex-and-rwmutex.html]

The Toilet Example (c) Copyright 2005, Niclas Winquist ;)

[There Are Many Ways To Safely Count. Bruno Calza](https://brunocalza.me/there-are-many-ways-to-safely-count/)

[Multicores and Publication Safety by Bartosz Milewski](https://bartoszmilewski.com/2008/08/04/multicores-and-publication-safety/)

[Dekker’s Algorithm Does not Work, as Expected by Jakob](https://jakob.engbloms.se/archives/65)

[Lock-free Programming: Dekker's Algorithm by Adrian de Mesquita](https://series1.github.io/blog/dekkers-algorithm/)

[Recursive (Re-entrant) Locks by Stephen Cleary](https://blog.stephencleary.com/2013/04/recursive-re-entrant-locks.html)

[An Illustrated Proof of the CAP Theorem by mwhittaker](https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/)

[Testing Distributed Systems for Linearizability by Anish Athalye](https://www.anishathalye.com/2017/06/04/testing-distributed-systems-for-linearizability/)

[Semaphores](https://www3.physnet.uni-hamburg.de/physnet/Tru64-Unix/HTML/APS33DTE/DOCU_010.HTM)

[Locks Aren't Slow; Lock Contention Is by Jeff Preshing](https://preshing.com/20111118/locks-arent-slow-lock-contention-is/)

[The Free Lunch Is Over. A Fundamental Turn Toward Concurrency in Software by Herb Sutter](www.gotw.ca/publications/concurrency-ddj.htm)

[Fibers under the magnifying glass](https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1364r0.pdf)

[Bell Labs and CSP Threads by Russ Cox](https://swtch.com/~rsc/thread/)

[Composing Specifications by Martin Abadi and Leslie Lamport](https://lamport.azurewebsites.net/pubs/abadi-composing.pdf)

### books

[Communicating Sequential Processes by C. A. R. Hoare](http://www.usingcsp.com/cspbook.pdf)

### papers

[ReLoC: A Mechanised Relational Logic for Fine-Grained Concurrency](https://iris-project.org/pdfs/2018-lics-reloc-final.pdf)

[Lockless algorithms for mere mortals by Jonathan Corbet](https://lwn.net/Articles/827180/)

[A Concurrent Window System by Rob Pike](https://swtch.com/~rsc/thread/cws.pdf)

[A surprise with mutexes and reference counts by Jonathan Corbet](https://lwn.net/Articles/575460/)

[Performance evaluation of distributed mutual exclusionPerformance evaluation of distributed mutual exclusion algorithms by Kenneth B Been](https://digitalscholarship.unlv.edu/cgi/viewcontent.cgi?article=1279&context=rtds)

THE MUTUAL EXCLUSION PROBLEM bY T. H. Bredt
http://i.stanford.edu/pub/cstr/reports/cs/tr/70/173/CS-TR-70-173.pdf

The Mutual Exclusion Problem. Part I: A Theory of Interprocess Communication by L. Lamport
https://www.ics.uci.edu/~cs230/reading/Lamport%20Mutual%20Exclusion%20Problem%20Part1.pdf

The Mutual Exclusion Problem Part II: Statement and Solutions by L. Lamport
https://www.microsoft.com/en-us/research/uploads/prod/2016/12/The-Mutual-Exclusion-Problem-Part-II.pdf

A Permission Based Hierarchical Algorithm for Mutual Exclusion by Mohammad Ashiqur Rahman
https://acyd.fiu.edu/wp-content/uploads/A-Permission-Based-Hierarchical-Algorithm-for-Mutual-Exclusion.pdf

Efficient Mutual Exclusion in Peer-to-Peer Systems by Moosa Muhammad, Adeep S. Cheema, and Indranil Gupta
https://dprg.cs.uiuc.edu/data/files/2005/grid05_poster.pdf

A Fast Mutual Exclusion Algorithm․ Leslie Lamport
https://www.ics.uci.edu/~cs230/reading/Lamport%20Fast%20Mutual%20Exclusion%20Algorithm.pdf

Mutex mutandis: understanding mutex types and attributes
https://www.2net.co.uk//tutorial/mutex_mutandis

Group Mutual Exclusion by Fetch-and-increment by Alex Aravind, Wim H. Hesselink
https://dl.acm.org/doi/10.1145/3309202

Efficient Single Writer Concurrency by Naama Ben-Davi  Guy E. Blelloch Yihan Sun Yuanhao Wei
https://www.researchgate.net/publication/324005771_Efficient_Single_Writer_Concurrency

Understanding and Effectively Preventing the ABA Problem in
Descriptor-based Lock-free Designs
https://www.stroustrup.com/isorc2010.pdf

Is Parallel Programming Hard, And, If So, What Can You Do About It? by Paul E. McKenney
https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.2011.01.02a.pdf

    [A Fast Mutual Exclusion Algorithm by Leslie Lamport](https://lamport.azurewebsites.net/pubs/fast-mutex.pdf)

    [A New Solution of Dijkstra's Concurrent Programming Problem by Leslie Lamport](https://lamport.azurewebsites.net/pubs/bakery.pdf)

    [The Parallel Execution of DO Loops by Leslie Lamport](https://lamport.azurewebsites.net/pubs/do-loops.pdf)

    [What is RCU, Fundamentally?](https://lwn.net/Articles/262464/)

    [What is RCU? Part 2: Usage](https://lwn.net/Articles/263130/)

    [RCU part 3: the RCU API](https://lwn.net/Articles/264090/)

    [User-space RCU](https://lwn.net/Articles/573424/)

    [READ-COPY UPDATE: USING EXECUTION HISTORY TO SOLVECONCURRENCY PROBLEMS. PAUL E. MCKENNEY JOHN D. SLINGWINE](http://www.rdrop.com/users/paulmck/RCU/rclockpdcsproof.pdf)

    [User-Level Implementations of Read-Copy UpdateMathieu Desnoyers, Paul E. McKenney, Alan S. Stern, Michel R. Dagenais and Jonathan Walpole](http://www.rdrop.com/users/paulmck/RCU/urcu-main-accepted.2011.08.30a.pdf)


    [Fast and Correct Load-Link/Store-Conditional Instruction Handling in DBTSystems](https://homepages.inf.ed.ac.uk/s1343145/USENIX2020.pdf)

    [Lockless patterns: an introduction to compare-and-swap by Paolo Bonzini](https://lwn.net/Articles/847973/)

    [Workspace Consistency: A Programming Model for Shared Memory Parallelism](https://www.academia.edu/3568009/Workspace_Consistency_A_Programming_Model_for_Shared_Memory_Parallelism)

    [Tapir: Embedding Fork-Join Parallelism into LLVM's Intermediate Representation](https://www.researchgate.net/publication/313019806_Tapir_Embedding_Fork-Join_Parallelism_into_LLVM%27s_Intermediate_Representation)

    [Testing Distributed Systems for Linearizability by Anish Athalye](https://www.anishathalye.com/2017/06/04/testing-distributed-systems-for-linearizability/)

    [Linearizability: A Correctness Condition for Concurrent Objects](http://www.calebgossler.com/notes/Papers/linearizability.html)

    [From Linearizability to CAP Theorem: A Brief Overview of Consistency Guarantees in Concurrent Programs by Udit kumar agarwal ](https://uditagarwal.in/from-linearizability-to-cap-theorem-a-brief-overview-of-consistency-guarantees-in-concurrent-programs/)

    [Perspectives on the CAP Theorem by Seth Gilbert, Nancy A. Lynch](https://groups.csail.mit.edu/tds/papers/Gilbert/Brewer2.pdf)

    [Modular Verification of Concurrency-Aware Linearizability by Nir Hemed, Noam Rinetzky, Viktor Vafeiadis](https://hal.archives-ouvertes.fr/hal-01207126/document)

    [Quasi-Linearizability: Relaxed Consistency For Improved Concurrency by Yehuda Afek, Guy Korland, and Eitan Yanovsky](http://mcg.cs.tau.ac.il/papers/opodis2010-quasi.pdf)

    [Monitors: An Operating System Structuring Concept](https://people.cs.uchicago.edu/~shanlu/teaching/33100_fa15/papers/hoare-monitors.pdf)

    [Wait-Free Synchronization by MAURICE HERLIHY](https://cs.brown.edu/~mph/Herlihy91/p124-herlihy.pdf)

    [Wait-Freedom with Advice by Carole Delporte-Gallet Hugues Fauconnier Eli Gafni Petr Kuznetsov](https://arxiv.org/pdf/1109.3056.pdf)

    [Effective Concurrency: Prefer Using Futures or Callbacks to Communicate Asynchronous Results](https://herbsutter.com/2010/08/27/effective-concurrency-prefer-using-futures-or-callbacks-to-communicate-asynchronous-results/)

    [Another Threading Post Index](https://cbloomrants.blogspot.com/2012/06/06-12-12-another-threading-post-index.html)

    [Scalable Reader-Writer Locks by Yossi Lev, Victor Luchangco Marek Olszewski](http://people.csail.mit.edu/mareko/spaa09-scalablerwlocks.pdf)

    [Fast mutual exclusion algorithms using read-modify-write and atomic read/write registers by Ting-Lu Huang](https://people.cs.nctu.edu.tw/~tlhuang/Papers/ICPADS98.pdf)

[Articles by Wirth](http://pascal.hansotten.com/niklaus-wirth/articles-on-pascal/)

[Debunking the 'Expensive Procedure Call' Myth, or, Procedure Call Implementations Considered Harmful, or, Lambda: The Ultimate GOTO by Guy Steele](https://dspace.mit.edu/handle/1721.1/5753)

Jeremy Gibbons: Publications
https://www.cs.ox.ac.uk/people/publications/date/Jeremy.Gibbons.html
https://www.cs.ox.ac.uk/softeng/Jeremy.Gibbons/publications.html

[Computability and Recursion by Robert I. Soare](http://www.people.cs.uchicago.edu/~soare/History/compute.pdf)

[The next 700 programming languages by P. J. Landin](https://www.cs.cmu.edu/~crary/819-f09/Landin66.pdf) [acm dl](https://dl.acm.org/doi/10.1145/365230.365257)

[Can Programming Be Liberated from the von Neumann Style? A Functional Style and Its Algebra of Programs by John Backus](https://dl.acm.org/doi/pdf/10.1145/359576.359579) [other source](http://worrydream.com/refs/Backus-CanProgrammingBeLiberated.pdf)

[You and Your Research by Richard Hamming](https://www.cs.virginia.edu/~robins/YouAndYourResearch.html)

[Programming With(out) the GOTO by B .M . Leavenworth](https://dl.acm.org/doi/pdf/10.1145/987361.987370)

Gerard J. Holzmann
http://spinroot.com/gerard/
https://scholar.google.com/citations?user=thNT9QYAAAAJ&hl=en
https://www.researchgate.net/profile/Gerard-Holzmann/3

John Reppy
https://scholar.google.com/citations?user=hz1S-asAAAAJ&hl=en

Richard Bird's publications
https://www.cs.ox.ac.uk/people/richard.bird/publications.html

Erik Meijer
https://scholar.google.com/citations?user=odFMpOYAAAAJ&hl=en

Jeremy Gibbons' Publications
https://www.cs.ox.ac.uk/people/jeremy.gibbons/publications/index.html

lambda papers
https://research.scheme.org/lambda-papers/
https://web.archive.org/web/20160510140804/http://library.readscheme.org/page1.html

John McCarthy's papers
http://www-formal.stanford.edu/jmc/

Philip Wadler's papers
https://homepages.inf.ed.ac.uk/wadler/
http://jmc.stanford.edu/articles/index.html

[Theorem for free! by Philip Wadler](https://dl.acm.org/doi/pdf/10.1145/99370.99404)

### lock free ds

[Lock-freedom without garbage collection](https://aturon.github.io/blog/2015/08/27/epoch/)

[Lock-Free Code: A False Sense of Security. Herb Sutter](https://web.archive.org/web/20081119213915/http://www.ddj.com/cpp/210600279?pgno=2)

[Lock-Free Queues by Petru Marginean](https://drdobbs.com/parallel/lock-free-queues/208801974)

[The World's Simplest Lock-Free Hash Table by Jeff Preshing](https://preshing.com/20130605/the-worlds-simplest-lock-free-hash-table/)

[Lock-Free Data Structures with Hazard Pointers by Andrei Alexandrescu, Maged Michael](http://erdani.org/publications/cuj-2004-12.pdf)

[Obstruction-Free Synchronization: Double-Ended Queues as an Example by Maurice Herlihy, Victor Luchangco, Mark Moir](https://cs.brown.edu/people/mph/HerlihyLM03/main.pdf)

[Wait-Free Queues With Multiple Enqueuers and Dequeuers by Alex Kogan, Erez Petrank](https://csaws.cs.technion.ac.il/~erez/Papers/wfquque-ppopp.pdf)

### atomic

[Lockless patterns: more read-modify-write operations. Paolo Bonzini](https://lwn.net/Articles/849237/)

[First Things First. scalability by Dmitry Vyukov](https://www.1024cores.net/home/lock-free-algorithms/first-things-first)

[Atomic vs. Non-Atomic Operations. Jeff Preshing](https://preshing.com/20130618/atomic-vs-non-atomic-operations/)

[An introduction to lockless algorithms by Paolo Bonzini](https://lwn.net/Articles/844224/)

[An Introduction to Lock-Free Programming by Jeff Preshing](https://preshing.com/20120612/an-introduction-to-lock-free-programming/)

[Lock-free multithreading with atomic operations by Internal Pointers](https://www.internalpointers.com/post/lock-free-multithreading-atomic-operations)

[Lockless patterns: an introduction to compare-and-swap by Paolo Bonzini](https://lwn.net/Articles/847973/)

[Introducing Mintomic: A Small, Portable Lock-Free API](https://preshing.com/20130505/introducing-mintomic-a-small-portable-lock-free-api/), [code](https://mintomic.github.io/lock-free/atomics/)

[C++ atomics and memory ordering by Bartosz Milewski](https://bartoszmilewski.com/2008/12/01/c-atomics-and-memory-ordering/)

[Weak vs. Strong Memory Models by Jeff Preshing](https://preshing.com/20120930/weak-vs-strong-memory-models/)

[Atomics in AArch64 by Jim Cownie](https://cpufun.substack.com/p/atomics-in-aarch64)

[Hardware Atomicity for Reliable Software Speculation by Naveen Neelakantam, Ravi Rajwar, Suresh Srinivas, Uma Srinivasan, Craig Zilles](https://pages.cs.wisc.edu/~rajwar/papers/hardware_atomicity.isca2007.pdf)

[Fine Grained Analysis of Read-Modify-Write Performance in NUMA Architectures by Naama Ben-David, Ziv Scully](https://www.cs.cmu.edu/afs/cs/academic/class/15740-s18/www/report/cas)

[Increasing the Size of Atomic Instruction Blocks using Control Flow Assertions by Sanjay J. Patel, Tony Tung, Satarupa Bose, Matthew M. Crum](https://www.eecg.utoronto.ca/~moshovos/ACA07/projectsuggestions/patel00increasing.pdf)

[Evaluating the Cost of Atomic Operations on Modern Architectures by Hermann Schweizer, Maciej Besta, and Torsten Hoefler](https://spcl.inf.ethz.ch/Publications/.pdf/atomic-bench.pdf)

[You Can Do Any Kind of Atomic Read-Modify-Write Operation by Jeff Preshing](https://preshing.com/20150402/you-can-do-any-kind-of-atomic-read-modify-write-operation/)

[Validating Memory Barriers and Atomic Instructions by Paul McKenney](https://lwn.net/Articles/470681/)

[Read-Modify-Write Networks by Panagiota Fatourou, Maurice Herlihy](https://cs.brown.edu/people/mph/FatourouH03/switch.pdf)

[Atomic Read-Modify-Write Operations are Unnecessary for Shared-Memory Work Stealing by Umut A. Acar, Arthur Charguéraud, Stefan Muller, Mike Rainey](https://inria.hal.science/hal-00910130/document)

### functional programming

(Higher-order Functions15-150:  Principles of Functional Programming. by Giselle Reis)[https://web2.qatar.cmu.edu/cs/15150/fall2018/lectures/13-higher-order-functions.pdf]

### thread

https://web.mit.edu/6.005/www/fa15/classes/20-thread-safety/#strategy_1_confinement

https://github.com/tmrts/go-patterns --lexical & adhoc confinement

http://www.thinkingparallel.com/2006/10/15/a-short-guide-to-mastering-thread-safety/

### self-hosting

[Setup your server with ease, you already have everything at home](https://yunohost.org/#/)

### fediverse

[Privacy research on Matrix.org. Max Dor](https://github.com/libremonde-org/paper-research-privacy-matrix.org)

[Run your digital services from your home](https://freedombox.org/)

[Federated, free, and/or open source software will not be chosen over proprietary software unless they overcome this hurdle by @snek_boi](https://lemmy.ml/post/346124)

ActivityPub: the present state, or why saving the 'worse is better' virus is both possible and important by Ariadne Conill
https://ariadne.space/2019/01/10/activitypub-the-present-state-or-why-saving-the-worse-is-better-virus-is-both-possible-and-important/

Re-Decentralizing the Web, For Good This Time by Ruben Verborgh
https://ruben.verborgh.org/articles/redecentralizing-the-web/

Challenges in Building a Decentralized Web
https://educatedguesswork.org/posts/challenges-web-decentralization/

The Rise and Demise of RSS by Two-Bit History
https://twobithistory.org/2018/12/18/rss.html

The AT Protocol is an open, decentralized network for building social applications.
https://atproto.com/

LitePub Overview
https://litepub.social/overview

trunk
https://communitywiki.org/trunk

Challenges in the Decentralised Web: The Mastodon Case by Aravindh Raman Sagar Joglekar1, Emiliano De Cristofaro2,3, Nishanth Sastry1, and Gareth Tyson3
https://arxiv.org/pdf/1909.05801

# json, protobuf

(Beating JSON performance with Protobuf by Bruno Krebs)[https://auth0.com/blog/beating-json-performance-with-protobuf/]

(Introduction to gRPC: A general RPC framework that puts mobile and HTTP/2 first (M.Atamel, R.Tsang)(youtube video))[https://youtu.be/hNFM2pDGwKI]

# pipeline

(Pipelines in Golang by  Richard Clayton)[https://rclayton.silvrback.com/pipelines-in-golang]

(Go Concurrency Patterns: Pipelines and cancellation by Sameer Ajmani)[https://go.dev/blog/pipelines]

### event loop

[Don't Block the Event Loop (or the Worker Pool)](https://nodejs.org/en/docs/guides/dont-block-the-event-loop/)

[The Node.js Event Loop. Nodejs.dev](https://nodejs.dev/learn/the-nodejs-event-loop)

[The Node.js Event Loop, Timers, and process.nextTick(). Nodejs.org](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/)

[Event Loop and the Big Picture — NodeJS Event Loop Part 1](https://blog.insiderattack.net/event-loop-and-the-big-picture-nodejs-event-loop-part-1-1cb67a182810)

[Timers, Immediates and Process.nextTick— NodeJS Event Loop Part 2](https://blog.insiderattack.net/timers-immediates-and-process-nexttick-nodejs-event-loop-part-2-2c53fd511bb3)

[Promises, Next-Ticks, and Immediates— NodeJS Event Loop Part 3](https://blog.insiderattack.net/promises-next-ticks-and-immediates-nodejs-event-loop-part-3-9226cbe7a6aa)

### thinkpad

[IBM ThinkPads in space](https://www.ibm.com/ibm/history/exhibits/space/space_thinkpad.html)

# problems, courses, tutorials

[A list of practical projects that anyone can solve in any programming language. karan](https://github.com/karan/Projects)

[dataquest. Learn data science by ~~watching videos~~ coding!](https://www.dataquest.io/)

[dataquest. sql fundamental](https://www.dataquest.io/blog/sql-fundamentals/)

[studytonight. Education simplified](https://www.studytonight.com/)

[Javatpoint](https://www.javatpoint.com/)

[Learn Git Branching](https://learngitbranching.js.org)

[codesdope](https://www.codesdope.com/)

[programiz](https://www.programiz.com/)

[dzone](https://dzone.com/)

[MIT OpenCourseWare.Engineering - Computer Science](https://ocw-origin.odl.mit.edu/courses/find-by-topic/#cat=engineering&subcat=computerscience&spec=algorithmsanddatastructures)

[The Open Source Computer Science Degree](https://github.com/ForrestKnight/open-source-cs)

[Path to a free self-taught education in Computer Science!](https://github.com/ossu/computer-science)

# technology, language, etc.

[A Brief, Incomplete, and Mostly Wrong History of Programming Languages by James Iry](http://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html)

[Code is not literature by Peter Seibel](https://gigamonkeys.com/code-reading/)

[rivescript](https://www.rivescript.com/docs/tutorial)

_RiveScript is a text-based scripting language meant to aid in the development of interactive chatbots. A chatbot is a software application that can communicate with humans using natural languages such as English in order to provide entertainment, services or just have a conversation._

[Industrial-Strength Natural Language Processing in Python](https://spacy.io/)

[The DOT Language](https://graphviz.org/doc/info/lang.html)

_Graphviz - Graph Visualization Software_

[A read-it-later service for RSS purists by Tobias Alexander Franke](https://www.tobias-franke.eu/log/2024/04/28/a-reader-service-for-rss-purists.html)


# python-specific

[django vs flask](https://testdriven.io/blog/django-vs-flask/)

[Python cost model](https://ocw-origin.odl.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/readings/python-cost-model/)

# articles, blogposts, opinion

[Execution in the Kingdom of Nouns by Steve Yegge](https://steve-yegge.blogspot.com/2006/03/execution-in-kingdom-of-nouns.html)

[What Does Haskell Have to Do with C++? by Bartosz Milewsk](https://bartoszmilewski.com/2009/10/21/what-does-haskell-have-to-do-with-c/)

[An Interview with A. Stepanov by Graziano Lo Russo](http://www.stlport.org/resources/StepanovUSA.html)

[Procedure vs. Function vs. Method vs. ? by Musings](https://stillat.com/blog/2014/07/19/procedure-vs-function-vs-method-vs/)

[The Forgotten History of OOP by Eric Elliott](https://medium.com/javascript-scene/the-forgotten-history-of-oop-88d71b9b2d9f)

[How Many x86-64 Instructions Are There Anyway? by stefan heule](https://stefanheule.com/blog/how-many-x86-64-instructions-are-there-anyway/)

[RSS, or why lack of developer imagination will be the end of the open web by Craig Maloney](http://decafbad.net/2020/08/11/rss-or-why-lack-of-developer-imagination-will-be-the-end-of-the-open-web/)

[Firmware Updates by mitxela](https://mitxela.com/rants)

[Basics of the Unix Philosophy by Eric Steven Raymond](http://catb.org/~esr/writings/taoup/html/ch01s06.html)

[3 tribes of programming by Joseph Gentle](https://josephg.com/blog/3-tribes/)

[Principles of UI, A Thread․ notes.yip.pe](https://notes.yip.pe/notes/notes/Principles%20of%20UI%2C%20A%20Thread.html)
 
[The Software Industry is Broken by Dmitry Kudryavtsev](https://www.yieldcode.blog/post/the-software-industry-is-broken)

[What You Miss By Only Checking GitHub by Sumana Harihareswara](https://www.harihareswara.net/posts/2022/what-you-miss-by-only-checking-github/)

[Sketchy Websites Save the Day by exozy](https://a.exozy.me/posts/sketchy-websites-save-the-day/)

[Please don't use Discord for FOSS projects by Drew DeVault](https://drewdevault.com/2021/12/28/Dont-use-Discord-for-FOSS.html)

[Your syntax highlighter is wrong by James Fisher](https://jameshfisher.com/2014/05/11/your-syntax-highlighter-is-wrong/)

[A Brief, Incomplete, and Mostly Wrong History of Programming Languages by  James Iry](https://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html)

[Fibonacci & L-grammars by David P. Medeiros](http://davidpmedeiros.com/fibonacci-l-grammars/)

[NPM & left-pad: Have We Forgotten How To Program? by David Haney](https://www.davidhaney.io/npm-left-pad-have-we-forgotten-how-to-program/)

[Program development by Stepwise Refinement by Niklaus Wirth](https://people.inf.ethz.ch/wirth/Articles/StepwiseRefinement.pdf)

[A plea for lean software by Niklaus Wirth](https://cr.yp.to/bib/1995/wirth.pdf)

[A clean start for the web by Tom MacWright](https://macwright.com/2020/08/22/clean-starts-for-the-web.html)

[How to Generate Random Timings for a Poisson Process by Jeff Preshing](https://preshing.com/20111007/how-to-generate-random-timings-for-a-poisson-process/)

[Mapping the whole internet with Hilbert curves by benjojo](https://blog.benjojo.co.uk/post/scan-ping-the-internet-hilbert-curve)

[An Interview with Brian Kernighan by Mihai Budiu](https://www.cs.cmu.edu/~mihaib/kernighan-interview/)

[Linus Torvalds Answers Your Questions 326](https://meta.slashdot.org/story/12/10/11/0030249/linus-torvalds-answers-your-questions)

[Worse Is Better by Richard P. Gabriel](https://www.dreamsongs.com/WorseIsBetter.html)

[The 'Story of Mel' Explained by James Seibel](https://jamesseibel.com/the-story-of-mel/)

[Donald Knuth on work habits, problem solving, and happiness](https://shuvomoy.github.io/blogs/posts/Knuth-on-work-habits-and-problem-solving-and-happiness/)

[Why Pascal is Not My Favorite Programming Language by Brian W. Kernighan](https://doc.cat-v.org/bell_labs/why_pascal/why_pascal_is_not_my_favorite_language.pdf)

[Things a Computer Scientist Rarely Talks About by Knuth](https://www-cs-faculty.stanford.edu/~knuth/things.html)

(Donald Knuth on work habits, problem solving, and happiness)[https://shuvomoy.github.io/blogs/posts/Knuth-on-work-habits-and-problem-solving-and-happiness/]

[An Essay on Programming by Niklaus Wirth](http://oberon2005.oberoncore.ru/paper/eth315.pdf)

https://www.dreamsongs.com/index.html

[The Yesterweb | Reclaiming the Internet](https://yesterweb.org/#manifesto)

https://goblin-heart.net/

[Rediscovering the Small Web by Parimal Satyal](https://neustadt.fr/essays/the-small-web/)

[Growing a Language by Guy L. Steele Jr.](https://www.cs.virginia.edu/~evans/cs655/readings/steele.pdf) [talk](https://youtu.be/_ahvzDzKdB0)

[The Rise of Worse is Better by Richard P. Gabriel](https://www.dreamsongs.com/RiseOfWorseIsBetter.html)

[The Humble Programmer by Edsger W. Dijkstra](https://www.cs.utexas.edu/~EWD/transcriptions/EWD03xx/EWD340.html)

[Worse is Better is Worse by Nickieben Bourbaki](https://www.dreamsongs.com/Files/worse-is-worse.pdf)

[A plea for lean software by Niklaus Wirth](https://people.inf.ethz.ch/wirth/Articles/LeanSoftware.pdf)

[Money Through Innovation Reconsidered | dreamsongs](https://www.dreamsongs.com/Files/Innovation.pdf)

[Software and Anarchy by Gnuxie Lulamoon and Hayley Patton](https://applied-langua.ge/software-and-anarchy.pdf)

### tools, design, etc.

https://polyhaven.com/

https://www.maxon.net/en/

https://www.grasshopper3d.com/

https://www.rhino3d.com/

https://wattwise.games/

https://www.keyshot.com/

https://opentoonz.github.io/e/

https://www.marvelousdesigner.com/?gad_source=1

https://monogame.net/

https://stylizedstation.com/

https://www.mixamo.com/#/

Inigo Quilez
https://iquilezles.org/

https://www.blenderkit.com/

https://blendswap.com/

http://mantaflow.com/

https://studio.blender.org/welcome/

https://static.makehumancommunity.org/

https://craftinginterpreters.com/

https://github.com/HackerPoet/NonEuclidean

https://develop.games/

### պատմություն

https://issuu.com/armenianinternationalmagazine/docs/september1990

[What Will Grow Out of a Pocket Full of Sunflower Seeds? by Slavoj Žižek](https://thephilosophicalsalon.com/what-will-grow-out-of-a-pocket-full-of-sunflower-seeds/)

[ԼԵՎՈՆ ՏԵՐ-ՊԵՏՐՈՍՅԱՆԸ ՎԵՐԱԴԱՌՆՈՒՄ Է](https://www.aravot.am/2002/04/13/795226/)

[Լեւոն Տեր֊Պետրոսյանի այսօրը](https://www.aravot.am/1998/07/15/660581/)

[Սա է իրականությունը](https://www.aravot.am/1995/10/17/601103/)

[Լևոն Տեր-Պետրոսյան. Սյունիքի նշանակությունը, 1996թ](https://youtu.be/iALDNN8J2cA)

[Լևոն Տեր-Պետրոսյանի ելույթը Համաժողովրդական շարժման 2-րդ կոնգրեսում, 02.05.2008թ.](https://www.youtube.com/watch?v=FuvmJmSfcwo)

[Վահե Գրիգորյան. Հակասությունը Սահմանադրության եւ մեկ կոնկրետ քաղաքական ուժի օրակարգի միջեւ է](https://www.ilur.am/%D5%BE%D5%A1%D5%B0%D5%A5-%D5%A3%D6%80%D5%AB%D5%A3%D5%B8%D6%80%D5%B5%D5%A1%D5%B6-%D5%B0%D5%A1%D5%AF%D5%A1%D5%BD%D5%B8%D6%82%D5%A9%D5%B5%D5%B8%D6%82%D5%B6%D5%A8-%D5%BD%D5%A1%D5%B0%D5%B4%D5%A1%D5%B6/)

[Վահե Գրիգորյան. Հանրապետության չորրորդ նախագահը դեռ չի ընտրվել](https://www.ilur.am/%D5%BE%D5%A1%D5%B0%D5%A5-%D5%A3%D6%80%D5%AB%D5%A3%D5%B8%D6%80%D5%B5%D5%A1%D5%B6-%D5%B0%D5%A1%D5%B6%D6%80%D5%A1%D5%BA%D5%A5%D5%BF%D5%B8%D6%82%D5%A9%D5%B5%D5%A1%D5%B6-%D5%B9%D5%B8%D6%80%D6%80%D5%B8/)

[Լեւոն Տեր֊Պետրոսյան․ Քաղաքական դիմանկար](https://www.aravot.am/2007/10/30/328955/)

[«Հարցազրույց Հայաստանի հետ». Վահե Գրիգորյան. Իշխանությունը միշտ պատկանելու է ժողովրդին](https://www.ilur.am/%D5%B0%D5%A1%D6%80%D6%81%D5%A1%D5%A6%D6%80%D5%B8%D6%82%D5%B5%D6%81-%D5%B0%D5%A1%D5%B5%D5%A1%D5%BD%D5%BF%D5%A1%D5%B6%D5%AB-%D5%B0%D5%A5%D5%BF-%D5%BE%D5%A1%D5%B0%D5%A5-%D5%A3%D6%80%D5%AB/)

[«Հարցազրույց Հայաստանի հետ»: Վահե Գրիգորյան. Սահմանադրությունն այլևս մեր պաշտպանության կարիքն ունի](https://www.ilur.am/%D5%B0%D5%A1%D6%80%D6%81%D5%A1%D5%A6%D6%80%D5%B8%D6%82%D5%B5%D6%81-%D5%B0%D5%A1%D5%B5%D5%A1%D5%BD%D5%BF%D5%A1%D5%B6%D5%AB-%D5%B0%D5%A5%D5%BF-%D5%BE%D5%A1%D5%B0%D5%A5-%D5%A3%D6%80%D5%AB-2/)

# decentralization/free software

[Why does decentralization matter](https://blog.joinmastodon.org/2018/12/why-does-decentralization-matter/)

# architecture

[Microservices and Unix Philosophy. assoc](https://assoc.tumblr.com/post/84115533294/microservices-and-unix-philosophy)

# papers

[D. E. Knuth. "Big omicron and big omega and big theta](https://dl.acm.org/doi/pdf/10.1145/1008328.1008329)

[Some Papers and books of P.J. Scott](https://www.site.uottawa.ca/~phil/papers/)

papers by Paul Hudak
https://scholar.google.com/citations?user=W6Mk2KwAAAAJ

https://omelkonian.github.io/

[Selected publications of Gadi Taubenfeld](https://faculty.runi.ac.il/gadi/publications.htm)

https://www.jwz.org/doc/worse-is-better.html
https://ewontfix.com/14/

https://users.umiacs.umd.edu/~hal/

https://lamport.azurewebsites.net/pubs/pubs.html

Anish Athalye
https://anish.io/

Mark P. Jones: Selected Publications
http://web.cecs.pdx.edu/~mpj/pubs.html

John Reppy's Publications
https://people.cs.uchicago.edu/~jhr/

https://oatd.org/

https://philarchive.org/

Prof. Dr. Anja Volk
https://webspace.science.uu.nl/~fleis102/

# etc

[Firefox, I love you but you’re bringing me down. Ryan Daniels](https://ryandaniels.ca/blog/firefox-i-love-you-but-youre-bringing-me-down/)

[location sharing serive](github.com/bilde2910/Hauk)

[open street map](https://umap.openstreetmap.fr/en/]

[science/math books](https://spot.colorado.edu/~dubin/bookmarks/index.html)

[An Interview with A. Stepanov by Graziano Lo Russo](http://www.stlport.org/resources/StepanovUSA.html)

[Computer history wiki](https://gunkies.org/wiki/Main_Page)

[how shazam works](http://coding-geek.com/how-shazam-works/)

[The Computer Language Benchmarks Game](https://benchmarksgame-team.pages.debian.net/benchmarksgame/)

[WTFPL – Do What the Fuck You Want to Public License](http://www.wtfpl.net/download/)

[Nachum Danzig's Home Page](http://www.danzig.jct.ac.il/)

[http://info.cern.ch - home of the first website](http://info.cern.ch/)

[typing is hard](https://typing-is-hard.ch/)

[Linus Torvalds on C++](http://harmful.cat-v.org/software/c++/linus)

[Simple and privacy-friendly alternative to Google Analytics](https://plausible.io/)

[Top arXiv papers](https://scirate.com/)

[papers-we-love](https://github.com/papers-we-love/papers-we-love)

### humor, jargon

[Some AI Koans. Hacker Folklore](http://www.catb.org/jargon/html/koans.html)

[15 new ways to catch a lion by John Barrington](https://gwern.net/doc/math/humor/1976-barrington.pdf)

# talks

[4 Programming Paradigms In 40 Minutes by Aja Hammerly](https://youtu.be/cgVVZMfLjEI)

[Why Isn't Functional Programming the Norm? by Richard Feldman](https://youtu.be/QyJZzq0v7Z4)

[Structured Programming by Dijikstra](www.youtube.com/watch?v=72RA6Dc7rMQ&list=PLE4A84616B0532460)

["The Mess We're In" by Joe Armstrong](https://youtu.be/lKXe3HUG2l4)

# algorithms

[A Fast Mutual Exclusion Algorithm by Leslie Lamport](https://ics.uci.edu/~cs237/reading/files/Fast%20Mutual%20Exclusion%20Algorithm.pdf)

[Algorithmics: towards programming as a mathematical activity](https://ir.cwi.nl/pub/268)

[Binary search trees](https://www.codesdope.com/course/data-structures-binary-search-trees/)

[big O cheat sheet](https://www.bigocheatsheet.com/)

[Optimal memory reallocation and the golden ratio by Chris Taylor](https://archive.ph/Z2R8w)

# go

[How are Go channels implemented?](https://stackoverflow.com/questions/19621149/how-are-go-channels-implemented)

[Golang: Concurrency: Monitors and Mutexes, A (light) Survey](https://medium.com/dm03514-tech-blog/golang-monitors-and-mutexes-a-light-survey-84f04f9b7c09)

[Head first into sync.Cond of Golang. Pinku Deb Nath](https://medium.com/@pinkudebnath/head-first-into-sync-cond-of-golang-be71779699b1)

[The Case For A Go Worker Pool](https://www.brandur.org/go-worker-pool)

[Concurrency in Golang And WorkerPool [Part 1]](https://app.hackernoon.com/drafts/e5dIjipLStSzXtOn6McQ?ref=hackernoon.com)

[Goroutine Leaks - The Forgotten Sender](https://www.ardanlabs.com/blog/2018/11/goroutine-leaks-the-forgotten-sender.html)

[Leaking Goroutines](https://www.openmymind.net/Leaking-Goroutines/)

[Several Cases of Goroutine Leaks in Golang](https://www.sobyte.net/post/2021-06/several-cases-of-goroutine-leaks-in-golang/)

[Go vs Node vs Rust vs Swift](https://grigio.org/go-vs-node-vs-rust-vs-swift/)

[Dissecting Go Binaries](https://www.grant.pizza/dissecting-go-binaries/)

[awesome go](https://awesome-go.com/)

[Interfaces in Go by Uday Hiwarale](https://medium.com/rungo/interfaces-in-go-ab1601159b3a)

[How To Use Go Interfaces by Chewxy](https://blog.chewxy.com/2018/03/18/golang-interfaces/)

[Interface naming convention in Golang by Lucas Do](https://medium.com/@dotronglong/interface-naming-convention-in-golang-f53d9f471593)

[The Laws of Reflection by Rob Pike](https://blog.golang.org/laws-of-reflection)

[Errors are values by Rob Pike](https://blog.golang.org/errors-are-values)

[Defer, Panic, and Recover by Andrew Gerrand](https://blog.golang.org/defer-panic-and-recover)

[Error Handling in Go by Martin Kühl](https://www.innoq.com/en/blog/golang-errors-monads/)

[Arrays, slices (and strings): The mechanics of 'append' by Rob Pike](https://blog.golang.org/slices)

### federation, activity pub, webfinger .. standards

[Webfinger](https://datatracker.ietf.org/doc/html/rfc7033)

[How to implement remote following for your ActivityPub project](https://www.hughrundle.net/how-to-implement-remote-following-for-your-activitypub-project/)

[An ActivityPub server implementation example | Python](https://github.com/tOkeshu/activitypub-example)

[honk](https://humungus.tedunangst.com/r/honk)

[Blushy-Crushy Fediverse Idol: A Chat with Lain about Pleroma by Sean Tilley](https://medium.com/we-distribute/blushy-crushy-fediverse-idol-a-chat-with-lain-about-pleroma-4ff578b99752)

### architecture

[Structuring Applications in Go. Ben Johnson](https://medium.com/@benbjohnson/structuring-applications-in-go-3b04be4ff091)

[Standard Package Layout. Ben Johnson](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1)


### c [but not c++ ] 

[Understanding lvalues and rvalues in C and C++](https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c)

[C++ Rvalue References: The Unnecessarily Detailed Guide by betaveros](https://blog.vero.site/post/rvalue-references)

[Massacring C Pointers by Wozniak](https://wozniak.ca/blog/2018/06/25/1/index.html)

[Pointers Are Complicated, or: What's in a Byte? by ralfj.de](https://www.ralfj.de/blog/2018/07/24/pointers-and-bytes.html)

[Pointers Are Complicated II, or: We need better language specs by ralfj.de](https://www.ralfj.de/blog/2020/12/14/provenance.html)

[Alloca and Realloc – Useful Tools, Not Ancient Relics. blog.demofox.org](https://blog.demofox.org/2013/09/09/alloca-and-realloc-useful-tools-not-ancient-relics/)

### closure

[Closure conversion: How to compile lambda by matthew might](https://matt.might.net/articles/closure-conversion/)

### functional programming

[Subject: Re: FP, OO and relations. Does anyone trump the others?](https://okmij.org/ftp/Scheme/oop-in-fp.txt) [archive](https://web.archive.org/web/20081226055307/http://okmij.org/ftp/Scheme/oop-in-fp.txt)

### folklore

[Real Programmers Don’t Use Pascal by Ed Post](https://www.usm.uni-muenchen.de/~hoffmann/roff/tmp/rpdup.pdf)

[Real Programmers Don't Use Pascal](https://infogalactic.com/info/Real_Programmers_Don%27t_Use_Pascal)

[More About Real Programmers](https://web.archive.org/web/20080419225755/http://www.suslik.org/Humour/Computer/Langs/real_prog2.html)

[The Story of Mel, a Real Programmer by Ed Nather](http://www.pbm.com/~lindahl/mel.html)

### discussions

[Using Goto in Linux Kernel Code](https://koblents.com/Ches/Links/Month-Mar-2013/20-Using-Goto-in-Linux-Kernel-Code/) [lkml](https://lkml.org/lkml/2003/1/12/126)

[GPL vs BSD, a matter of sustainability (matusiak.eu). HN](https://news.ycombinator.com/item?id=5187235)

### memory

[Examining the Stack](https://web.archive.org/web/20210414194505/http://kirste.userpage.fu-berlin.de/chemnet/use/info/gdb/gdb_7.html)

[Function Call Stack Examples by Chris Kauffman](https://cs.gmu.edu/~kauffman/cs222/stack-demo.html)

[Where the top of the stack is on x86 by Eli Bendersky](https://eli.thegreenplace.net/2011/02/04/where-the-top-of-the-stack-is-on-x86/)

[Stack frames by Paul Krzyzanowski](https://people.cs.rutgers.edu/~pxk/419/notes/frames.html)

[Understanding the Stack](https://web.archive.org/web/20130225162302/http://www.cs.umd.edu/class/sum2003/cmsc311/Notes/Mips/stack.html)

[Spatial and Temporal Locality by Sergey Slotin](https://en.algorithmica.org/hpc/external-memory/locality/)

### call stack

[Writing a Linux Debugger Part 8: Stack unwinding by Sy Brand](https://blog.tartanllama.xyz/writing-a-linux-debugger-unwinding/)

[Deep Wizardry: Stack Unwinding by Josh Haberman](https://blog.reverberate.org/2013/05/deep-wizardry-stack-unwinding.html)

### tail call

[Tail Recursion. Lean](https://leanprover.github.io/functional_programming_in_lean/programs-proofs/tail-recursion.html)

[Tail Call Optimisation in C++ by Andy Balaam](https://accu.org/journals/overload/20/109/balaam_1914/)

[Performance of recursive functions (and when not to use them) by Guillaume Bogard](https://guillaumebogard.dev/posts/recursion-performance/)

[Tail Call Optimisation and C / C++ by Tristan Penman](https://tristanpenman.com/blog/posts/2018/12/08/tail-call-optimisation-and-c-cpp/)

[How Tail Call Optimization Works by Evan Klitzke](https://eklitzke.org/how-tail-call-optimization-works) [discussion. HN](https://news.ycombinator.com/item?id=25681362)

[The Story of Tail Call Optimizations in Rust by Sean Chen](https://dev.to/seanchen1991/the-story-of-tail-call-optimizations-in-rust-35hf)

[Why Object-Oriented Languages Need Tail Calls. reflection](http://www.eighty-twenty.org/2011/10/01/oo-tail-calls)

[How Tail Call Optimization Works by Evan Klitzke](https://eklitzke.org/how-tail-call-optimization-works)

[How Tail Call Optimization Works (eklitzke.org). HN](https://news.ycombinator.com/item?id=25681362)

[Procedures and the Processes They Generate. Interactive Structure and Interpretation of Computer Programs](https://xuanji.appspot.com/isicp/1-2-procedures.html) 
 
[Debunking the 'Expensive Procedure Call' Myth, or, Procedure Call Implementations Considered Harmful, or, Lambda: The Ultimate GOTO by Guy Steele](https://dspace.mit.edu/handle/1721.1/5753)

### recursion papers

[Primivie Recursive Functions by Raphael M. Robinson](https://www.ams.org/journals/bull/1947-53-10/S0002-9904-1947-08911-4/S0002-9904-1947-08911-4.pdf)

[A THEOREM ON GENERAL RECURSIVE FUNCTIONS by SHIH-CHAO LIU](https://web.archive.org/web/20180724031832id_/https://www.ams.org/journals/proc/1960-011-02/S0002-9939-1960-0130175-8/S0002-9939-1960-0130175-8.pdf)

[GENERAL RECURSION VIA COINDUCTIVE TYPES by VENANZIO CAPRETTA](https://arxiv.org/pdf/cs/0505037v4)

[structured programming with goto statement by Donald Knuth](https://dl.acm.org/doi/pdf/10.1145/356635.356640)

### assembly

[Stack and Functions](https://azeria-labs.com/functions-and-the-stack-part-7/)

### pinetime

[A look at Pine64: Pinetime by DarrowAtreides](https://itsmoss.com/2021/12/16/a-look-at-pine64-part-1-the-good/)

[PineTime: a smartwatch for open-source software by Sam Sloniker](https://lwn.net/Articles/941796/)

[Thoughts on my PineTime by Akseli Lahtinen](https://akselmo.dev/posts/thoughts-on-my-pinetime/)

### etc.

[HOW TO BUILD AN ECONOMIC MODEL IN YOUR SPARE TIME](https://people.ischool.berkeley.edu/~hal/Papers/how.pdf)

https://www.thisman.org/

https://int10h.org/oldschool-pc-fonts/fontlist/

[Fonts in Use](https://fontsinuse.com/)

[oldschool pc fonts](https://int10h.org/oldschool-pc-fonts/)

https://sr.ht/

[profanity. console based xmpp client](https://profanity-im.github.io/)

[Social Coding](https://coding.social/)

Drupal. ActivityPub, IndieWeb reader
https://realize.be/
https://www.drupal.org/project/reader

[Wobbly. Web-Based Gemini browser](https://warmedal.se/~wobbly/)

[fafi. gemini browser](https://git.sr.ht/~soapdog/fafi-browser)

[Scuttlebutt Protocol Guide](https://ssbc.github.io/scuttlebutt-protocol-guide/index.html)

[yt-dlc. media downloader and library for various sites](https://github.com/blackjack4494/yt-dlc)

[yt-dlp. A feature-rich command-line audio/video downloader](https://github.com/yt-dlp/yt-dlp)

[HARFANG. 3D Real Time Visualization Tools](https://www.harfang3d.com/en_US/)

https://github.com/Swordfish90/cool-retro-term

[Joplin. note-taking app](https://joplinapp.org/)

[Manush. menu shell](https://github.com/illuria/manush)

[ArchiveBox. Open source self-hosted web archiving](https://archivebox.io/)

### pine

[apps for postmarketos](https://wiki.postmarketos.org/wiki/Applications_by_category)

[awesome pinetime](https://github.com/sethitow/awesome-pinetime)

[pt-connect](https://github.com/kmsgli/pt-connect/tree/main)

[TT-time](https://github.com/TT-392/TT-time)

[pinetime-updater](https://github.com/lupyuen/pinetime-updater/tree/master)

### retro computing

https://gunkies.org/wiki/PDP-8

http://chdickman.com/pdp8/

http://www.bitsavers.org/pdf/dec/pdp8/pdp8/

https://www.studiodessuantbone.com/articles/what-is-pdp-8-in-computer-architecture/

https://en.wikipedia.org/wiki/ZX_Spectrum

[Soothing Sounds of the IBM PC AT](https://youtu.be/eSNqzTwHiuU)

# teco

[Text Editor and COrector](https://almy.us/teco.html)

[TECO Editor Information](http://www.pdp8online.com/editors/teco/teco.shtml)

[TECO reference manual](http://bitsavers.org/pdf/dec/pdp8/os8/AA-H608A-TA_os8teco_mar79.pdf)

[A new portable TECO based on TECO-32 V40](https://github.com/tesneddon/tecox)

[TECOC - TECO text editor](https://github.com/blakemcbride/TECOC)

[The Beginnings of TECO by Dan Murphy](https://opost.com/tenex/anhc-31-4-anec.pdf)

### audiobooks

https://typeaudiobooks.com/

https://primeaudiobook.com/

https://bookaudio.online/

### art

https://stane-jagodic.com/

https://libraryofbabel.info/

Carrie Mae Weems
https://www.carriemaeweems.net/

https://www.juliahetta.com/

https://americanartist.us/

Rick Owens minimalist home in Concordia
https://www.vosgesparis.com/2022/05/rick-owens-minimalist-home-in-concordia.html

https://www.jeffbark.com/

https://worksofanais.com/

https://www.sara-phillips.com/

http://www.ccru.net/index.htm

Joanne Nam

(Dante and Virgil | William-Adolphe Bouguereau)[https://www.wikiart.org/en/william-adolphe-bouguereau/dante-and-virgil-1850]

The Flaying of Marsyas by Titian
https://www.wikiart.org/en/titian/the-flaying-of-marsyas-1576

[Anatomical Pieces | Théodore Géricault](https://www.wikiart.org/en/theodore-gericault/anatomical-pieces-1819-1)

Gallowgate Lard by Ken Currie

[BLACK GOOEY UNIVERSE by american artist](https://americanartist.us/works/black-gooey-universe)

https://www.bryancharnley.info/

https://thevaultoftheatomicspaceage.tumblr.com/

https://americanartist.us/

Mark Wigley: Anarchitecture 101.5--Cutting Matta-Clark (September 27, 2017)
https://www.youtube.com/watch?v=zvrlCmo_Mnc

How to Develop Film with Beer by Vincent Moschetti
https://petapixel.com/2017/03/15/st-patricks-photo-tip-develop-film-beer/

The House That Ludwig Built
https://www.thelondonlist.com/culture/wittgenstein-house

“Mourning Sickness”, February 2014 by Kia LaBeija

Kia LaBeija’s Eleven, 2015.

William Utermohlen
https://www.williamutermohlen.com/

PRINCIPLES OF ANARCHITECTURE
https://alienistmanifesto.wordpress.com/2019/03/01/principles-of-anarchitecture/
https://alienistmanifesto.wordpress.com/wp-content/uploads/2019/03/principles-of-anarchitecture_interior-ministry_2019-book-2.pdf

Digital Constructivism by Stane Jagodič
https://stane-jagodic.com/Digital-Constructivism

https://www.juliahetta.com/

Carrie Mae Weems
https://www.carriemaeweems.net/

https://pinouts.org/

https://www.nationalgalleries.org/search

Yildiz Moran
https://www.yildizmoran.com.tr/

Lauren Tepfer
http://www.laurentepfer.com/

https://www.nvardyerkanian.com/

https://alecsoth.com/photography/

https://www.frrr.dk/

https://www.mattstuart.com/

https://joeredski.com/

https://www.jaspervandenende.com/

https://arnoldnewman.com/index.html

http://www.asharmitage.com/

https://www.elliotterwitt.com/

https://irvingpenn.org/

https://www.yajisuke.net/

https://rinkokawauchi.com/en/

Michiko Kon
https://www.pgi.ac/en/

Rashod Taylor
https://www.rashodtaylor.com/

Tobia Montanari
https://www.tobiamontanari.com/

https://www.secondname.agency/

http://www.daniel-arnold.org/

https://whentosaynothing.com/

https://www.michaelwriston.com/

https://www.davidlachapelle.com/

https://www.timwalkerphotography.com/

https://siegfried-hansen.de/

Faizal Westcott
https://faizalwestcott.com/

https://www.maryellenmark.com/

http://www.alexeytitarenko.com/

https://www.delotbinierephotography.com/

https://mashaivashintsova.com/

Désirée Dolron
https://www.desireedolron.com/

https://jaspervdj.be/photos.html

### gemini, web, etc.

https://thewebisfucked.com/

[The rise and fall of the Gopher protocol by Tim Gihring](https://www.minnpost.com/business/2016/08/rise-and-fall-gopher-protocol/)

[web3 is a stupid idea by](https://timdaub.github.io/2020/09/08/web3/)

[web3 is not decentralization](https://invisibleup.com/articles/38/)

[Keep the web free, say no to Web3](https://yesterweb.org/no-to-web3/)

The Gemini protocol seen by this HTTP client person by Daniel Stenberg
https://daniel.haxx.se/blog/2023/05/28/the-gemini-protocol-seen-by-this-http-client-person/

In solidarity with Library Genesis and Sci-Hub
https://custodians.online/

https://web0.small-web.org/

https://gemlog.blue/

### blogs, etc.

[Laboria Cuboniks | xenofeminism](https://laboriacuboniks.net/)

[ԶՕՏ | քննական հարթակ](https://zot.am/)

https://esoteric.codes/

ՀԱՅԿԱԿԱՆ ԱՐԴԻՈՒԹՅՈՒՆՆԵՐԻ ՈՒՍՈՒՄՆԱՍԻՐՄԱՆ ՆԱԽԱԳԻԾ
https://modernities.am/

https://jaspervdj.be/

### literature, essays, philosophy

[Baudrillard as Post-Marxist @PlasticPills by Pill Pod Philosophy & Critical Theory Podcast](https://youtu.be/qXXOIGav_5s?si=vCiNPz9yJskboEMd)

[Paulrillard: reading Paul though Baudrillard by Mike Grimshaw](https://baudrillardstudies.ubishops.ca/paulrillard-reading-paul-though-baudrillard/)

[Simulation and Hyperreality: Misconceptions about Baudrillard (Ft. @TheoryPhilosophy by 1Dime Radio](https://youtu.be/sEbbQJKX6Zw)

[Baudrillard and the Forgetting of Death as a Challenge by Dr. Ahmet Dağ](https://baudrillard-scijournal.com/baudrillard-and-the-forgetting-of-death-as-a-challenge/)

[Simulation, Simulacra and Solaris by Haladyn, Julian Jason and Jordan, Miriam](https://openresearch.ocadu.ca/id/eprint/1327/1/Haladyn_Solaris_2010.pdf)

ICONOGRAPHY AND POSTMODERNITY by Steven Grimwood

[From the Subject of Desire to the Object of Seduction: Image – Imagination – Imaginary by Christoph Wulf](https://baudrillardstudies.ubishops.ca/from-the-subject-of-desire-to-the-object-of-seduction-image-imagination-imaginary/)

[The Matrix Decoded: Le Nouvel Observateur Interview With Jean Baudrillard](https://baudrillardstudies.ubishops.ca/the-matrix-decoded-le-nouvel-observateur-interview-with-jean-baudrillard/)

[Տրամաբանա-փիլիսոփայական տրակտակ (Tractatus logico-philosophicus) (հատվածներ). Լյուդվիգ Վիտգենշտայն](https://roubicon.media/philosophy/446/)

[ՈՒԽՏԻ ՄԻԱԲԱՆՈՒԹՅՈՒՆԸ, կամ կոլեկտիվ ներկայության պարադոքսները․ Աշոտ Ոսկանյան](https://roubicon.media/history/491/)

[Four puzzling paragraphs: Frege on ‘≡’ and ‘=. María de Ponte, Kepa Korta and John Perry](https://www.mariadeponte.com/uploads/3/1/6/6/31661835/semiotica_frege.pdf)

[Did Frege Believe Frege’s Principle? by FRANCIS JEFFRY PELLETIER](https://www.sfu.ca/~jeffpell/papers/FregesPrinciplePublished.pdf)

[On sense and reference by Gottlob Frege](http://www.ub.edu/grc_logos/files/1263459298-Sense&Reference.pdf)

[Homo Trickster կամ էսթետիզմի ծուղակները․ Աշոտ Ոսկանյան](https://roubicon.media/philosophy/401/)

[Պասկալ. Մաքս Պիկարդ | Արա Գուրզադյան](https://roubicon.media/philosophy/381/)

[Գաղափարների մասին ընդհանրապես․ Իմմանուել Կանտ](https://roubicon.media/philosophy/292/)

[ Mark Fisher: The Slow Cancellation Of The Future | lecture](https://www.youtube.com/watch?v=aCgkLICTskQ)

[Ջոն Դրայդեն. Հատվածներ «Օվիդիուսի նամակների նախաբանից»](https://roubicon.media/literature/654/)

[Debate | Noam Chomsky & Michel Foucault - On human nature](https://youtu.be/3wfNl2L0Gf8)

[Սա ծխամորճ չէ․ Միշել Ֆուկո](https://roubicon.media/art/408/)

[Եղիշե Չարենց](https://roubicon.media/literature/260/)

[Why Baudrillard Hated the Matrix | And why he was wrong](https://www.thelivingphilosophy.com/why-baudrillard-hated-the-matrix/)

https://www.lacan.com/

[Slavoj Zizek: NO SEX, PLEASE, WE'RE POST-HUMAN!](https://www.lacan.com/nosex.htm)

[Հայոց արդիականացման պարադոքսներն ու սովորականությունը․ տուն-տիեզերքի և օջախի մարտահրավերները․ Վարդան Ազատյան | նախաբան](https://modernities.am/%d5%b0%d5%a1%d5%b5%d5%b8%d6%81-%d5%a1%d6%80%d5%a4%d5%ab%d5%a1%d5%af%d5%a1%d5%b6%d5%a1%d6%81%d5%b4%d5%a1%d5%b6-%d5%ba%d5%a1%d6%80%d5%a1%d5%a4%d5%b8%d5%bd%d6%84%d5%b6%d5%a5%d6%80%d5%b6-%d5%b8%d6%82/)

[Տեսողության սխրանքը․ Սուզան Զոնթագ | photobio](https://photobio.wordpress.com/2011/11/06/%d5%bf%d5%a5%d5%bd%d5%b8%d5%b2%d5%b8%d6%82%d5%a9%d5%b5%d5%a1%d5%b6-%d5%bd%d5%ad%d6%80%d5%a1%d5%b6%d6%84%d5%a8/)

[Virginia Woolf’s voice, 1937 – "Craftsmanship" (with the closing lines), BBC broadcast / subtitled](https://youtu.be/zcbY04JrMaU)

[անկախ գրադարանների ցանց](http://libraries.am/)

[Billy-Ray Belcourt](https://billy-raybelcourt.com/)

https://arar.sci.am/dlibra

https://playaudiobooks.com/

https://cdaudiobook.com/

https://bookaudiobooks.com/

https://www.bbc.co.uk/sounds/category/audiobooks

https://www.graphicaudiointernational.net/

https://primeaudiobook.com/

https://typeaudiobooks.com/

http://honedieliebende.blogspot.com/

https://cdaudiobook.com/

https://stephenkingaudiobooks.club/

[Being -- Thinking -- Writing Jean Baudrillard by Rachel K. Ward and Jeremy Fernando](https://journals.uvic.ca/index.php/ctheory/article/view/14960/5858)

# podcasts

https://www.philosophizethis.org/podcasts

### graphics

[Rome in 3D](https://www.relivehistoryin3d.com/projects/rome-in-3d/)

### music

[A computational lens into how music characterizes genre in film by Benjamin MaID1, Timothy Greer1, Dillon Knox, Shrikanth Narayanan](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0249957)

Mauceri, Frank X. 1997. From Experimental Music to Musical Experiment

Functionally Generating Music Structures with Prefix Trees by Erlend Kilvik Hoff
https://ntnuopen.ntnu.no/ntnu-xmlui/bitstream/handle/11250/2787894/no.ntnu:inspera:76427839:20996928.pdf?sequence=1

Casey Reas: Form + Code in Design, Art, and Architecture: A guide to Computational Aesthetics

Designing Sound by Andy Farnell (Author)

Rhyme and Reason: An Introduction to Minimalist Syntax by Juan Uriagereka, Massimo Piatelli-Palmarini

Formalized Music: Thought and Mathematics in Composition by Iannis Xenakis

Language-Based Music:  Cognition and Computation by Jordan Alexander Ackerman
https://escholarship.org/uc/item/8w69z3bt

Jacob, Bruce L. (1996), Algorithmic composition as a model of creativity
http://www.ee.umd.edu/~blj/algorithmic_composition/algorithmicmodel.html

Alpern, Adam (1995), Techniques for algorithmic composition of music. http://alum.hampshire.edu/~adaF92/algocomp/algocomp95.html. Hampshire College

Berger, Jonathan (2004), Who cares if it listens? An essay on creativity, expectations, and computational modeling of listening to music. In Virtual Music: Computer Synthesis of Musical Style. David Cope. MIT Press. 584 pp.

Burns, Kristine, H. (1997), Algorithmic composition, a definition. http://music.dartmouth.edu/~wowem/hardware/algorithmdefinition.html. Florida International University.

### music & programming languages

[Alda. text-based programming language for music composition](https://alda.io/) [code](https://github.com/alda-lang/alda)

[Oz programming language](https://codedocs.org/what-is/oz-programming-language) [A History of the Oz Multiparadigm Language](https://dl.acm.org/doi/pdf/10.1145/3386333)

[Csound. audio programming language](https://learn.bela.io/using-bela/languages/csound/) [code](https://github.com/csound/csound)

[ChucK](https://chuck.cs.princeton.edu/)

[Making music with text[ure] | Alex McLean](https://slab.org/)

https://github.com/truj/midica

### music papers

Evolutionary Computation for Computer Music by Sjaak J¨obses
http://sonology.org/wp-content/uploads/2019/10/ECforCM-SJob-FINAL.pdf

[A functional model of jazz improvisation by Donya Quick, Kelland Thomas](https://www.researchgate.net/publication/334757619_A_functional_model_of_jazz_improvisation)

[GGA-MG: Generative Genetic Algorithm for Music Generation by Majid Farzaneh. Rahil Mahdian Toroghi](https://arxiv.org/pdf/2004.04687.pdf)

### books

Shultis, Christopher | Silencing the Sounded Self: John Cage and the American Experimental Tradition

### resources

https://www.rosegardenmusic.com/

https://puredata.info/

https://en.wikipedia.org/wiki/MUSIC-N

http://www.algorithmiccomposer.com/

https://algocomp.blogspot.com/

Real Time Interactive Music in Haskell
https://www.youtube.com/watch?v=9xfOslqAz7U

Composing (Music) in Haskell - Stuart Popejoy
https://youtu.be/Jmw6LLNQQfs

Making Algorithmic Music
https://youtu.be/9Fg54XAr044

# midi

[Understanding MIDI: A Painless Tutorial on Midi Format by H.M. de Oliveira, R.C. de Oliveira](https://arxiv.org/abs/1705.05322)

# euterpea

[Euterpea](https://www.euterpea.com/) [code | hsom](https://github.com/Euterpea/HSoM) [code | euterpea](https://github.com/Euterpea/Euterpea)

[The Haskell School of Music by Paul Hudak](https://www.cs.yale.edu/homes/hudak-paul/CS431F08/HaskoreSoeV-0.3.pdf)

[Making Music with Haskell From Scratch. Tsoding | video](https://youtu.be/FYTZkE5BZ-0) [code](https://github.com/tsoding/haskell-music)

[Music, Haskell... and Westeros](https://declension.net/posts/2017-08-08-music-haskell-and-westeros/)

[euterpea sandbox](https://github.com/declension/euterpea-sandbox?tab=readme-ov-file)

[Real-time interactive music in Haskell by  Paul Hudak, Donya Quick, Mark Santolucito, Daniel Winograd-Cort](https://www.researchgate.net/publication/299871824_Real-time_interactive_music_in_Haskell)

[Live music programming in Haskell by Henning Thielemann](https://arxiv.org/abs/1303.5768)

[Drew Krause: Introduction to Algorithmic Composition](https://vimeo.com/54910233)

[Making Algorithmic Music with Euterpea (Keynote) by Donya Quick](https://www.researchgate.net/publication/335919954_Making_Algorithmic_Music_with_Euterpea_Keynote)

[A sensitivity analysis of the MUSIC algorithm by B. Friedlander](https://ieeexplore.ieee.org/document/60105)

# tools, resources

https://www.donyaquick.com/

https://hackage.haskell.org/package/hmt

https://github.com/donya/Kulitta/

[The Yale Haskell Group](https://web.archive.org/web/20150424035418/http://haskell.cs.yale.edu/)

http://super-fluid.github.io/heqet/

https://github.com/tkuriyama/hsom

Timidity

https://github.com/abstractmachines/algorithm-music-transposition

[Tidal](https://tidalcycles.org/) [Tidal Practice by Left Adjoint](https://www.youtube.com/playlist?list=PLa_MGpdaZU2XNBdnvAAKV8DzAxpt0Sr6Y)

https://wiki.haskell.org/Haskore

https://hackage.haskell.org/packages/tag/sound

https://music-suite.github.io/docs/ref/#music-suite

https://patchstorage.com/

https://github.com/music-suite/music-suite

https://lilypond.org/

http://mozart2.org/

https://github.com/mozart

https://csound.com/

https://tidalcycles.org/

https://supercollider.github.io/

https://faust.grame.fr/

http://overtone.github.io/

# books, papers, etc.

[Generative Jazz by Donya Quick](https://www.donyaquick.com/generative-jazz/)

Programming for Musicians and Digital Artists: Creating music with ChucK by Ajay Kapur, Perry R. Cook), Spencer Salazar, Ge Wang


### photography

[Automatic exposure algorithms for digital photography by  Jarosław Bernacki](https://link.springer.com/article/10.1007/s11042-019-08318-1)

### lisp

[Lisp: Good News, Bad News, How to Win Big by Richard P. Gabriel](https://www.dreamsongs.com/Files/LispGoodNewsBadNews.pdf) [abstract](https://www.dreamsongs.com/WIB.html)

# projects

https://github.com/robert-strandh/SICL

### people

[Robert Strandh’s research](https://www.researchgate.net/scientific-contributions/Robert-Strandh-6613693)

[The Feynman Lectures](https://www.feynmanlectures.caltech.edu/)

### oberon

[The Programming Language Oberon-2 by H. Mössenböck, N. Wirth](https://cseweb.ucsd.edu/~wgg/CSE131B/oberon2.htm)

[Oberon-2, a hi-performance alternative to C++ by Günter Dotzel](https://folk.ntnu.no/haugwarb/Programming/Oberon/oberon_vs_cpp_II.htm)

[ActiveOberon based operating system (a2 aka aos aka Bluebottle OS)](https://github.com/cubranic/oberon-a2)

[The LLVM-based Modula-2 compiler](https://github.com/redstar/m2lang)


[Wirth, N. (1977). Modula: A language for modular multiprogramming. Software: Practice and Experience](https://sci-hub.ru/10.1002/spe.4380070102)

[Oberon – The Overlooked Jewel by Michael Franz](https://people.cis.ksu.edu/~danielwang/Investigation/System_Security/download.pdf)

[Project Oberon. The Design of an Operating System, a Compiler, and a Computer by Niklaus Wirth, Jürg Gutknecht](https://people.inf.ethz.ch/wirth/ProjectOberon/PO.System.pdf)

[The Oberon+ Programming Language](https://oberon-lang.github.io/)

https://oberon.org/en

[Oberon RTS. Embedded Oberon for Real-time Systems](https://oberon-rts.org/)

[The ModulaTor. Oberon-2 and Modula-2 Technical Publication](http://www.modulaware.com/mdlt35.htm)

[Adding Concurrency to the Oberon System](https://www.researchgate.net/publication/220271786_Adding_Concurrency_to_the_Oberon_System)

[Mostly Oberon by R. S. Doiel](https://rsdoiel.github.io/blog/2020/04/11/Mostly-Oberon.html)

https://modula3.github.io/

[Object Oberon. An object-oriented extension of Oberon by Mössenböck, Hanspeter; Templ, Josef; Griesemer, Robert](https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/68697/eth-3204-01.pdf)

[Modular Programming Versus Object Oriented Programming (The Good, The Bad and the Ugly) by Stéphane Richard (Mystikshadows)](http://petesqbsite.com/sections/express/issue17/modularversusoop.html)

[Less is more. Why Oberon beats mainstream in complex applications by F V Tkachov](https://norayr.am/papers/A109-38-Less_is_more._Why_Oberon_beats_mainstream_in_complex_applications.pdf)

[Why Not Oberon? | StackSmith](gemini://gemini.ctrl-c.club/~stack/gemlog/2022-08-29.oberon.gmi)

[Do the Fish Really Need Remote Control? A Proposal for Self-Active Objects in Oberon by Jürg Gutknecht](https://dl.acm.org/doi/10.5555/645652.665357)

[The Circumstances in which Modular Programming becomes the Favor Choice by Novice Programmers by Ilana Lavy, Rashkovits Rami](https://www.mecs-press.org/ijmecs/ijmecs-v10-n7/IJMECS-V10-N7-1.pdf)

[Oberon System Implemented on a Low-Cost FPGA Board - Papers We Love #011](https://www.youtube.com/watch?v=8K84aG72Dw8)

[Interview with Niklaus Wirth | Michael Holzheu](https://www.youtube.com/watch?v=OEmMx55SF8U)

[Niklaus Wirth — a Pioneer of Computer Science by Gustav Pomberger, Hanspeter Mössenböck, Peter Rechenberg](http://pascal.hansotten.com/uploads/books/art1.pdf)

### projects

[Miniflux | minimalist and opinionated feed reader](https://miniflux.app/)

https://sr.ht/~lioploum/

[openring |  A webring for static site generators](https://git.sr.ht/~sircmpwn/openring)

Collection of microcontroller projects
https://github.com/aykevl/things

https://github.com/io-core/qemu-risc6

System building tools for the Project Oberon 2013 and Extended Oberon operating systems
https://github.com/andreaspirklbauer/Oberon-building-tools

https://swtch.com/

https://0.30000000000000004.com/

https://512kb.club/

https://www.abduct.com/

https://github.com/dspinellis

https://github.com/dspinellis/unix-history-repo

https://bellard.org/

https://karthikkaranth.me/art/

https://everynoise.com/engenremap.html

https://10kb.neocities.org/

https://caffeine.wiki/

https://niconiconi.neocities.org/

https://github.com/josStorer/selfhostedAI

https://github.com/KoboldAI/KoboldAI-Client

Carpalx - keyboard layout optimizer
https://mk.bcgsc.ca/carpalx/

[Ronin |  Experimental Graphics Terminal](https://hundredrabbits.github.io/Ronin/) [code](https://github.com/hundredrabbits/Ronin)

[Light Pattern](http://lightpattern.info/)

### system

https://diveintosystems.org/

https://programmingdesignsystems.com/introduction/

### podcasts

[ANTIC The Atari 8-bit Podcast by Randy Kindig, Kay Savetz, Brad Arnold](http://feeds.libsyn.com/45000/rss)

[Language Therapy with Dr. K](https://armenian.usc.edu/language-therapy-with-dr-k/)

[Better Fuji Photos by John Peltier](https://anchor.fm/s/d588c608/podcast/rss)

https://anarchitecture.podbean.com/

https://whyarecomputers.com/

https://notrelated.xyz/ | Not Related! A Big-Braned Podcast

[Codepunk - Programming, Technology, and the Digital Lifestyle](https://codepunk.io/)

https://www.artistsandhackers.org/Community-Memory

https://99percentinvisible.org/

https://overthinkpodcast.com/

### programming books & blogs

https://en.algorithmica.org/

https://bartoszmilewski.com/

https://existentialtype.wordpress.com/

Daniel Winograd‑Cort
https://www.danwc.com/

https://www.dreamsongs.com/index.html

### xmpp

https://libervia.org/

[Libervia Components: XMPP](https://libervia.org/__b/doc/backend/components.html)

### esoteric languages

[TMG](https://en.m.wikipedia.org/wiki/TMG_%28language%29)

[Rabbit: A compiler for Scheme](https://en.wikisource.org/wiki/Rabbit:_A_Compiler_for_Scheme)

[Newsqueak: A Language for Communicating with Mice by Rob Pike](https://swtch.com/~rsc/thread/newsqueak.pdf)

[Chialisp | based on Lisp that is used on the Chia blockchain to dictate how and when coins can be spent](https://chialisp.com/)

[The Limbo Programming Language by Dennis M. Ritchie](https://www.vitanuova.com/inferno/papers/limbo.html)

[Alef User’s Guide by Bob Flandrena](https://swtch.com/~rsc/thread/ug.pdf)

[Alef Language Reference Manual Phil Winterbottom](https://swtch.com/~rsc/thread/alef.pdf)

[Vale](https://vale.dev/)

[Ale | Lisp Environment](https://www.ale-lang.org/)

[Cyclone | safe dialect of C](http://cyclone.thelanguage.org/)

### type theory, language design

[Linear types can change the world. Philip Wadler](https://cs.ioc.ee/ewscs/2010/mycroft/linear-2up.pdf)

[Homotopy Type Theory: Vladimir Voevodsky - Computerphile](https://www.youtube.com/watch?v=v5a5BYZwRx8)

[Benjamin C. Pierce](https://www.cis.upenn.edu/~bcpierce/)

[Luca Cardelli](https://scholar.google.com/citations?user=npBTgSsAAAAJ&hl=en)

### papers, videos

[Spin | Verifying Multi-threaded Software](http://spinroot.com/spin/whatispin.html)

[Computer Science ∩ Mathematics (Type Theory) - Computerphile](https://youtu.be/qT8NyyRgLDQ)

[Assigning meanings to programs by Robert W. Floyd](https://people.eecs.berkeley.edu/~necula/Papers/FloydMeaning.pdf)

[On the Expressive Power of Programming Languages by Matthias Felleisen](https://jgbm.github.io/eecs762f19/papers/felleisen.pdf)

### math

[Fixed-point theorems and economic analysis by Herbert E. Scarf](http://dido.econ.yale.edu/~hes/pub/fixed%20point%20theorems.pdf)

[Frege’s Theorem and Foundations for Arithmetic](https://plato.stanford.edu/entries/frege-theorem/)

[Compositionality: its historic context by Theo M.V. Janssen](https://pure.uva.nl/ws/files/1159272/105371_HandbookJanssen.pdf)

On the anti-mechanist arguments based on Godel's theorem by Stanislaw Krajewski

### category theory

[Monads in Category Theory for Laymen by Andy Shiue](https://andyshiue.github.io/functional/programming/2017/02/06/monad.html)

[Monadic Parsing in Haskell by Graham Hutton, Erik Meijer](https://people.cs.nott.ac.uk/pszgmh/pearl.pdf)

[Category Theory in Life by Eugenia Cheng](https://youtu.be/ho7oagHeqNc)

[A Categorical View of Computational Effects by Emily Riehl](https://youtu.be/6t6bsWVOIzs)

[Monads in Haskell and Category Theory by Samuel Grahn](http://uu.diva-portal.org/smash/get/diva2:1369286/FULLTEXT01.pdf)

[Applied Category Theory Monads and Haskell by Duarte Maia](https://math.uchicago.edu/~dmaia/documents/applied_cats.pdf)

[Category Theory for the Working Hacker by Philip Wadler](https://www.youtube.com/watch?v=V10hzjgoklA)

[Category Theory by Tom LaGatta](https://www.youtube.com/watch?v=o6L6XeNdd_k)

### lambda calculus

[Intro to hacking with the lambda calculus by L Rudolf L](https://www.lesswrong.com/posts/D4PYwNtYNwsgoixGa/intro-to-hacking-with-the-lambda-calculus)

### esoteric.codes

[Null Programs and the Uninscribed](https://esoteric.codes/blog/null-programs-and-the-uninscribed)

[Unnecessary and Kallisti: Purely Conceptual Languages](https://esoteric.codes/blog/unnecessary-purely-conceptual-languages)

[What Programming Language Would Yoko Ono Create? by 21F05](https://esoteric.codes/blog/what-programming-language-would-yoko-ono-write)

[Checkout by 15D06](https://esoteric.codes/blog/checkout-coding-in-layers)

[Suicide Linux | 18C08](https://esoteric.codes/blog/suicide-linux)

[c0d3.Attorney | 18N12](https://esoteric.codes/blog/a-malbolge-mystery-c0d3-attorney)

[Make Your Hard Drive Infinite With These Three File Systems | 20L02](https://esoteric.codes/blog/make-your-hard-drive-infinite-with-these-three-file-systems)

[On tokens & vocabulary: AAAAAAAAAAAAAA!!!! vs Ook! | 14S13](https://esoteric.codes/blog/on-tokens-vocabulary-aaaaaaaaaaaaaa-vs-ook)

[Esoprogramming and Computational Idealism | 20M00](https://esoteric.codes/blog/esoprogramming-and-computational-idealism)

[Open and Shut | 20G13](https://esoteric.codes/blog/open-and-shut) [code](https://github.com/veggiedefender/open-and-shut/)

[projects](https://esoteric.codes/projects)

[Oou: The Insane Language | 16D00](https://esoteric.codes/blog/oou-the-insane-language)

[in:verse | 20N00](https://esoteric.codes/blog/inverse) [editor](https://editor.inverse.website/)

[Gottlob: Write Code in Frege's Concept Notation | 20G03](https://esoteric.codes/blog/gottlob-write-code-in-freges-concept-notation)

[Chef and the Aesthetics of Multicoding | 20E00](https://esoteric.codes/blog/chef-multicoding-esolang-aesthetics)

[Interview with 100 Rabbits | 21C07](https://esoteric.codes/blog/100-rabbits)

### ai papers

[Concepts of logical AI by John McCarthy](http://www-formal.stanford.edu/jmc/concepts-ai.pdf)

[Notes on Self-Awareness by John McCarthy](http://www-formal.stanford.edu/jmc/selfaware.pdf)

[Towards a Mathematical Science of Computation by J. McCarthy](http://www-formal.stanford.edu/jmc/towards.pdf)

[Programs with Common Sense by John McCarthy](http://www-formal.stanford.edu/jmc/mcc59.pdf)

[SIMPLE DETERMINISTIC FREE WILL by John McCarthy](http://www-formal.stanford.edu/jmc/freewill2.pdf)

[SOME PHILOSOPHICAL PROBLEMS FROM THE STANDPOINT OF ARTIFICIAL INTELLIGENCE by John McCarthy and Patrick J. Hayes](http://www-formal.stanford.edu/jmc/mcchay69.pdf)

[FREE WILL—EVEN FOR ROBOTS by John McCarthy](http://www-formal.stanford.edu/jmc/freewill.pdf)

### esoteric languages, formal languages, etc.

[Zonnon Language](https://zonnon.org/)

[Nial](https://www.nial-array-language.org/ndocs/intro/chapter1.html) [Nial | APL wiki](https://aplwiki.com/wiki/Nial)

[io](https://iolanguage.org/index.html)

[Address programming language](https://www.semanticscholar.org/topic/Address-programming-language/2424071) [syntax example](https://pub.sergev.org/doc/to-Donald-Knuth-about-Address-Programming.pdf) [talk](https://youtu.be/ztcS5hwx9-Q)

[Communicating Sequential Processes by C. A. R. Hoare](http://www.usingcsp.com/cspbook.pdf)

AMPL (A Mathematical Programming Language)

Programming Computable Functions

[Elephant 2000: A Programming Language Based on Speech Acts | John McCarthy](http://www-formal.stanford.edu/jmc/elephant.pdf)

[Almost Perfect Artifacts Improve only in Small Ways: APL is more French than English by Alan J. Perlis](https://www.jsoftware.com/papers/perlis78.htm)

[The Implementation of Newsqueak by Rob Pike](https://swtch.com/~rsc/thread/newsquimpl.pdf)

[Kitten](https://kittenlang.org/)

[The Hare programming language](https://harelang.org/)

[Piet](https://esolangs.org/wiki/Piet#:~:text=Piet%20is%20a%20stack%2Dbased,together%20with%20some%20unusual%20operations)

[Forth](https://www.forth.com/wp-content/uploads/2018/01/Starting-FORTH.pdf)

[esoteric.codes](https://esoteric.codes/)

### verification of programs, proving

[TLA+](https://lamport.azurewebsites.net/tla/tla.html)

[LSTS](https://github.com/andrew-johnson-4/LSTS)

[FStar](https://github.com/FStarLang/FStar)

[Lean Prover](https://github.com/leanprover/lean3) [tutorial](https://leanprover.github.io/functional_programming_in_lean/title.html)

[Verifying Haskell Programs Using Constructive Type Theory by Andreas Abel Marcin Benke Ana Bove John Hughes Ulf Norell](https://www.mimuw.edu.pl/~ben/Papers/monadic.pdf)

[(Programming Languages) in Agda = Programming (Languages in Agda) by Philip Wadler](https://youtu.be/9yplm_dsQHE)

[Comparison of Two Theorem Provers: Isabelle/HOL and Coq by Artem Yushkovskiy](https://arxiv.org/abs/1808.09701v2)

### category theory

[Category Theory: an abstract setting for analogy and comparison by R. Brown and T.Porter](https://groupoids.org.uk/pdffiles/Analogy-and-Comparison.pdf)

# resources, books

[Category Theory by Julia Goedecke](http://www.julia-goedecke.de/pdf/CategoryTheoryNotes.pdf)

[The interaction between category theory and set theory by Andreas Blass](https://dept.math.lsa.umich.edu/~ablass/interact.pdf)

### parser

[Which Parsing Approach? by Laurence Tratt](https://tratt.net/laurie/blog/2020/which_parsing_approach.html)

[Parsing With Haskell (Part 1): Lexing With Alex by Heitor Toledo Lassarote de Paula](https://serokell.io/blog/lexing-with-alex)

[Parsing With Haskell (Part 2): Parsing With Happy by Heitor Toledo Lassarote de Paula](https://serokell.io/blog/parsing-with-happy)

[megaparsec. tutorial](https://markkarpov.com/tutorial/megaparsec.html)

[Attention Is All You Need by Ashish Vaswani](https://arxiv.org/pdf/1706.03762)

[Faster and Smaller N -Gram Language Models by Adam Pauls Dan Klein](http://nlp.cs.berkeley.edu/pubs/Pauls-Klein_2011_LM_paper.pdf)

### drawing

https://www.interstice.cloud/

https://github.com/Acly/krita-ai-diffusion

https://docs.krita.org/en/resources_page.html#id2
