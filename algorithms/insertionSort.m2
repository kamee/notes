MODULE InsertionSort;
 CONST LENGTH = 5;
 TYPE
	array = ARRAY LENGTH OF INTEGER;
 VAR i, j, key: INTEGER;
 arr: array;
 BEGIN
   arr[0] := 1;
   arr[1] := 3;
   arr[2] := 6;
   arr[3] := 2;
   arr[4] := 5;

   FOR i := 2 TO LEN(arr) DO
	key := arr[i];
	j := i - 1;
	i := i + 1;
	WHILE (j >=0) & (arr[j] > key) DO
	  arr[i+1] := arr[j];
	  j := j - 1;
	END;
	arr[j+1] := key;
   END;

END InsertionSort.
