MODULE HeapSort;
 IMPORT Out;
 TYPE array = ARRAY 15 OF INTEGER;
 VAR largest, left, right, i, n: INTEGER;
 arr: array;

PROCEDURE swap(a, b, temp: INTEGER);
 BEGIN
	temp := a;
	a := b;
	b := temp;
 END swap;

 PROCEDURE heapify(arr, n, i: INTEGER);
 BEGIN
	largest := i;
	left  := 2*i + 1;
	right := 2*i + 2;

	IF left < n & arr[left] > arr[largest] THEN
		largest := left;
	ELSIF right < n & arr[right] > arr[largest] THEN
		largest := right;
	END;

	IF largest # i THEN
		swap(arr[i], arr[largest]);
		heapify(arr, n, largest);
	END;
 END heapify;

 PROCEDURE heapSort(arr, n: INTEGER);
 BEGIN
	FOR i := (n DIV 2)-1 TO 0 DO
		heapify(arr, n, i);
	END;
 END heapSort;
 
  BEGIN
	n := LEN(arr) DIV LEN(arr[0]);

END HeapSort.
