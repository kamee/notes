### functions

[Haskell Functions by Carl Burch](http://www.cburch.com/books/hsfun/)

[dollar sign](https://typeclasses.com/featured/dollar)

[Category: The Essence of Composition by Bartosz Milewski](https://bartoszmilewski.com/2014/11/04/category-the-essence-of-composition/)

### partial application

[Avoiding partial functions. wiki haskell](https://wiki.haskell.org/Avoiding_partial_functions)

### projects

[A lambda calculus interpreter in Haskell by Jamesh Fisher](https://jameshfisher.com/2018/03/15/a-lambda-calculus-interpreter-in-haskell/)

[Haskell-Euterpea-SevenNationArmy-tutorial. MaciejWanat](https://github.com/MaciejWanat/Haskell-Euterpea-SevenNationArmy-tutorial/tree/master)

### closure

[RE: What's so cool about Scheme?](http://people.csail.mit.edu/gregs/ll1-discuss-archive-html/msg03277.html)