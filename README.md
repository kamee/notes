# notes

այս պահոցում/շտեմարանում հաւաքելու եմ տարբեր յղումներ եւ նոթեր, որոնք կուտակւում են ուսումնասիրելու/սովորելու ընթացքում եւ որոնք երեւի օգտակար կարող են լինել այլոց։

***

### language design

[In Go, pointers (mostly) don't go with slices in practice. HN](https://news.ycombinator.com/item?id=28344938)

[Why C has Pointers by Andrew Hardwick](http://duramecho.com/ComputerInformation/WhyCPointers.html)

[Energy efficiency of programming languages by Marian's Garden](https://www.sendung.de/2022-07-24/programming-languages-energy-efficiency/)

[Less is exponentially more  by rob pike](https://commandcenter.blogspot.com/2012/06/less-is-exponentially-more.html)

[Dynamic Languages are Static Languages by Robert Harper](https://existentialtype.wordpress.com/2011/03/19/dynamic-languages-are-static-languages/)

### networking

(HTTP Signatures. draft-cavage-http-signatures-12)[https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12]

### concurrency

(Re: proc fs and shared pids: There is NO reason to think that "threads" and "processes" are separate
entities. Linus)[https://lkml.iu.edu/hypermail/linux/kernel/9608/0191.html]

[Mutex vs. Semaphores – Part 1: Semaphores by Niall Cooling](https://blog.feabhas.com/2009/09/mutex-vs-semaphores-%e2%80%93-part-1-semaphores/)

[Mutex vs. Semaphores – Part 3 (final part): Mutual Exclusion Problems by Niall Cooling](https://blog.feabhas.com/2009/10/mutex-vs-semaphores-%E2%80%93-part-3-final-part-mutual-exclusion-problems/)

(1024cores. Dmitry Vyukov blog)[https://www.1024cores.net/home]

(Mutex vs Atomic. CoffeeBeforeArch)[https://coffeebeforearch.github.io/2020/08/04/atomic-vs-mutex.html]

(Comparing the performance of atomic, spinlock and mutex)[https://demin.ws/blog/english/2012/05/05/atomic-spinlock-mutex/]

(You Can Do Any Kind of Atomic Read-Modify-Write Operation. Jeff Preshing)[https://preshing.com/20150402/you-can-do-any-kind-of-atomic-read-modify-write-operation/]

(The Trouble With Locks. C/C++ Users Journal, 23(3), March 2005.)[http://gotw.ca/publications/mill36.htm]

(The difference between mutex and rwmutex)[http://programmer.help/blogs/difference-between-mutex-and-rwmutex.html]

The Toilet Example (c) Copyright 2005, Niclas Winquist ;)

[There Are Many Ways To Safely Count. Bruno Calza](https://brunocalza.me/there-are-many-ways-to-safely-count/)

[Multicores and Publication Safety by Bartosz Milewski](https://bartoszmilewski.com/2008/08/04/multicores-and-publication-safety/)

[Dekker’s Algorithm Does not Work, as Expected by Jakob](https://jakob.engbloms.se/archives/65)

[Lock-free Programming: Dekker's Algorithm by Adrian de Mesquita](https://series1.github.io/blog/dekkers-algorithm/)

[Recursive (Re-entrant) Locks by Stephen Cleary](https://blog.stephencleary.com/2013/04/recursive-re-entrant-locks.html)

[An Illustrated Proof of the CAP Theorem by mwhittaker](https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/)

[Testing Distributed Systems for Linearizability by Anish Athalye](https://www.anishathalye.com/2017/06/04/testing-distributed-systems-for-linearizability/)

[Semaphores](https://www3.physnet.uni-hamburg.de/physnet/Tru64-Unix/HTML/APS33DTE/DOCU_010.HTM)

[Locks Aren't Slow; Lock Contention Is by Jeff Preshing](https://preshing.com/20111118/locks-arent-slow-lock-contention-is/)

[The Free Lunch Is Over. A Fundamental Turn Toward Concurrency in Software by Herb Sutter](www.gotw.ca/publications/concurrency-ddj.htm)

[Fibers under the magnifying glass](https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1364r0.pdf)

[Bell Labs and CSP Threads by Russ Cox](https://swtch.com/~rsc/thread/)

### atomic

[Lockless patterns: more read-modify-write operations. Paolo Bonzini](https://lwn.net/Articles/849237/)

[First Things First. scalability by Dmitry Vyukov](https://www.1024cores.net/home/lock-free-algorithms/first-things-first)

[Atomic vs. Non-Atomic Operations. Jeff Preshing](https://preshing.com/20130618/atomic-vs-non-atomic-operations/)

[An introduction to lockless algorithms by Paolo Bonzini](https://lwn.net/Articles/844224/)

[An Introduction to Lock-Free Programming by Jeff Preshing](https://preshing.com/20120612/an-introduction-to-lock-free-programming/)

[Lock-free multithreading with atomic operations by Internal Pointers](https://www.internalpointers.com/post/lock-free-multithreading-atomic-operations)

[Lockless patterns: an introduction to compare-and-swap by Paolo Bonzini](https://lwn.net/Articles/847973/)

[Introducing Mintomic: A Small, Portable Lock-Free API](https://preshing.com/20130505/introducing-mintomic-a-small-portable-lock-free-api/), [code](https://mintomic.github.io/lock-free/atomics/)

### functional programming

(Higher-order Functions15-150:  Principles of Functional Programming. by Giselle Reis)[https://web2.qatar.cmu.edu/cs/15150/fall2018/lectures/13-higher-order-functions.pdf]

### thread

https://web.mit.edu/6.005/www/fa15/classes/20-thread-safety/#strategy_1_confinement

https://github.com/tmrts/go-patterns --lexical & adhoc confinement

http://www.thinkingparallel.com/2006/10/15/a-short-guide-to-mastering-thread-safety/

### self-hosting

[Setup your server with ease, you already have everything at home](https://yunohost.org/#/)

### fediverse

[Privacy research on Matrix.org. Max Dor](https://github.com/libremonde-org/paper-research-privacy-matrix.org)

[Run your digital services from your home](https://freedombox.org/)

[Federated, free, and/or open source software will not be chosen over proprietary software unless they overcome this hurdle by @snek_boi](https://lemmy.ml/post/346124)

# json, protobuf

(Beating JSON performance with Protobuf by Bruno Krebs)[https://auth0.com/blog/beating-json-performance-with-protobuf/]

(Introduction to gRPC: A general RPC framework that puts mobile and HTTP/2 first (M.Atamel, R.Tsang)(youtube video))[https://youtu.be/hNFM2pDGwKI]

# pipeline

(Pipelines in Golang by  Richard Clayton)[https://rclayton.silvrback.com/pipelines-in-golang]

(Go Concurrency Patterns: Pipelines and cancellation by Sameer Ajmani)[https://go.dev/blog/pipelines]

### event loop

[Don't Block the Event Loop (or the Worker Pool)](https://nodejs.org/en/docs/guides/dont-block-the-event-loop/)

[The Node.js Event Loop. Nodejs.dev](https://nodejs.dev/learn/the-nodejs-event-loop)

[The Node.js Event Loop, Timers, and process.nextTick(). Nodejs.org](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/)

[Event Loop and the Big Picture — NodeJS Event Loop Part 1](https://blog.insiderattack.net/event-loop-and-the-big-picture-nodejs-event-loop-part-1-1cb67a182810)

[Timers, Immediates and Process.nextTick— NodeJS Event Loop Part 2](https://blog.insiderattack.net/timers-immediates-and-process-nexttick-nodejs-event-loop-part-2-2c53fd511bb3)

[Promises, Next-Ticks, and Immediates— NodeJS Event Loop Part 3](https://blog.insiderattack.net/promises-next-ticks-and-immediates-nodejs-event-loop-part-3-9226cbe7a6aa)

### thinkpad

[IBM ThinkPads in space](https://www.ibm.com/ibm/history/exhibits/space/space_thinkpad.html)

# problems, courses, tutorials

[A list of practical projects that anyone can solve in any programming language. karan](https://github.com/karan/Projects)

[dataquest. Learn data science by ~~watching videos~~ coding!](https://www.dataquest.io/)

[dataquest. sql fundamental](https://www.dataquest.io/blog/sql-fundamentals/)

[studytonight. Education simplified](https://www.studytonight.com/)

[Javatpoint](https://www.javatpoint.com/)

[Learn Git Branching](https://learngitbranching.js.org)

[codesdope](https://www.codesdope.com/)

[programiz](https://www.programiz.com/)

[dzone](https://dzone.com/)

[MIT OpenCourseWare.Engineering - Computer Science](https://ocw-origin.odl.mit.edu/courses/find-by-topic/#cat=engineering&subcat=computerscience&spec=algorithmsanddatastructures)

[The Open Source Computer Science Degree](https://github.com/ForrestKnight/open-source-cs)

[Path to a free self-taught education in Computer Science!](https://github.com/ossu/computer-science)

# technology, language, etc.

[rivescript](https://www.rivescript.com/docs/tutorial)

_RiveScript is a text-based scripting language meant to aid in the development of interactive chatbots. A chatbot is a software application that can communicate with humans using natural languages such as English in order to provide entertainment, services or just have a conversation._

[Industrial-Strength Natural Language Processing in Python](https://spacy.io/)

[The DOT Language](https://graphviz.org/doc/info/lang.html)

_Graphviz - Graph Visualization Software_


# python-specific

[django vs flask](https://testdriven.io/blog/django-vs-flask/)

[Python cost model](https://ocw-origin.odl.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/readings/python-cost-model/)

# articles, blogposts

[An Interview with A. Stepanov by Graziano Lo Russo](http://www.stlport.org/resources/StepanovUSA.html)

[Procedure vs. Function vs. Method vs. ? by Musings](https://stillat.com/blog/2014/07/19/procedure-vs-function-vs-method-vs/)

[The Forgotten History of OOP by Eric Elliott](https://medium.com/javascript-scene/the-forgotten-history-of-oop-88d71b9b2d9f)

[How Many x86-64 Instructions Are There Anyway? by stefan heule](https://stefanheule.com/blog/how-many-x86-64-instructions-are-there-anyway/)

[RSS, or why lack of developer imagination will be the end of the open web by Craig Maloney](http://decafbad.net/2020/08/11/rss-or-why-lack-of-developer-imagination-will-be-the-end-of-the-open-web/)

[Firmware Updates by mitxela](https://mitxela.com/rants)

[Basics of the Unix Philosophy by Eric Steven Raymond](http://catb.org/~esr/writings/taoup/html/ch01s06.html)

[3 tribes of programming by Joseph Gentle](https://josephg.com/blog/3-tribes/)

[Principles of UI, A Thread․ notes.yip.pe](https://notes.yip.pe/notes/notes/Principles%20of%20UI%2C%20A%20Thread.html)
 
[The Software Industry is Broken by Dmitry Kudryavtsev](https://www.yieldcode.blog/post/the-software-industry-is-broken)

[What You Miss By Only Checking GitHub by Sumana Harihareswara](https://www.harihareswara.net/posts/2022/what-you-miss-by-only-checking-github/)

[Sketchy Websites Save the Day by exozy](https://a.exozy.me/posts/sketchy-websites-save-the-day/)

[Please don't use Discord for FOSS projects by Drew DeVault](https://drewdevault.com/2021/12/28/Dont-use-Discord-for-FOSS.html)

[Your syntax highlighter is wrong by James Fisher](https://jameshfisher.com/2014/05/11/your-syntax-highlighter-is-wrong/)

[A Brief, Incomplete, and Mostly Wrong History of Programming Languages by  James Iry](https://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html)

[Fibonacci & L-grammars by David P. Medeiros](http://davidpmedeiros.com/fibonacci-l-grammars/)

[NPM & left-pad: Have We Forgotten How To Program? by David Haney](https://www.davidhaney.io/npm-left-pad-have-we-forgotten-how-to-program/)

[Program development by Stepwise Refinement by Niklaus Wirth](https://people.inf.ethz.ch/wirth/Articles/StepwiseRefinement.pdf)

[A plea for lean software by Niklaus Wirth](https://cr.yp.to/bib/1995/wirth.pdf)

[A clean start for the web by Tom MacWright](https://macwright.com/2020/08/22/clean-starts-for-the-web.html)

[How to Generate Random Timings for a Poisson Process by Jeff Preshing](https://preshing.com/20111007/how-to-generate-random-timings-for-a-poisson-process/)

[Mapping the whole internet with Hilbert curves by benjojo](https://blog.benjojo.co.uk/post/scan-ping-the-internet-hilbert-curve)

[An Interview with Brian Kernighan by Mihai Budiu](https://www.cs.cmu.edu/~mihaib/kernighan-interview/)

[Linus Torvalds Answers Your Questions 326](https://meta.slashdot.org/story/12/10/11/0030249/linus-torvalds-answers-your-questions)

[Worse Is Better by Richard P. Gabriel](https://www.dreamsongs.com/WorseIsBetter.html)

[The 'Story of Mel' Explained by James Seibel](https://jamesseibel.com/the-story-of-mel/)

[Donald Knuth on work habits, problem solving, and happiness](https://shuvomoy.github.io/blogs/posts/Knuth-on-work-habits-and-problem-solving-and-happiness/)

# decentralization/free software

[Why does decentralization matter](https://blog.joinmastodon.org/2018/12/why-does-decentralization-matter/)

# architecture

[Microservices and Unix Philosophy. assoc](https://assoc.tumblr.com/post/84115533294/microservices-and-unix-philosophy)

# papers

https://www.jwz.org/doc/worse-is-better.html
https://ewontfix.com/14/


# etc

[Firefox, I love you but you’re bringing me down. Ryan Daniels](https://ryandaniels.ca/blog/firefox-i-love-you-but-youre-bringing-me-down/)

[location sharing serive](github.com/bilde2910/Hauk)

[open street map](https://umap.openstreetmap.fr/en/]

[science/math books](https://spot.colorado.edu/~dubin/bookmarks/index.html)

[An Interview with A. Stepanov by Graziano Lo Russo](http://www.stlport.org/resources/StepanovUSA.html)

[Computer history wiki](https://gunkies.org/wiki/Main_Page)

[how shazam works](http://coding-geek.com/how-shazam-works/)

[The Computer Language Benchmarks Game](https://benchmarksgame-team.pages.debian.net/benchmarksgame/)

[WTFPL – Do What the Fuck You Want to Public License](http://www.wtfpl.net/download/)

[Nachum Danzig's Home Page](http://www.danzig.jct.ac.il/)

[http://info.cern.ch - home of the first website](http://info.cern.ch/)

[typing is hard](https://typing-is-hard.ch/)

[Linus Torvalds on C++](http://harmful.cat-v.org/software/c++/linus)

[Simple and privacy-friendly alternative to Google Analytics](https://plausible.io/)

[Top arXiv papers](https://scirate.com/)

[papers-we-love](https://github.com/papers-we-love/papers-we-love)

### humor, jargon

[Some AI Koans. Hacker Folklore](http://www.catb.org/jargon/html/koans.html)

[15 new ways to catch a lion by John Barrington](https://gwern.net/doc/math/humor/1976-barrington.pdf)

# talks

[4 Programming Paradigms In 40 Minutes by Aja Hammerly](https://youtu.be/cgVVZMfLjEI)

[Why Isn't Functional Programming the Norm? by Richard Feldman](https://youtu.be/QyJZzq0v7Z4)

[Structured Programming by Dijikstra](www.youtube.com/watch?v=72RA6Dc7rMQ&list=PLE4A84616B0532460)

["The Mess We're In" by Joe Armstrong](https://youtu.be/lKXe3HUG2l4)

# algorithms

[Binary search trees](https://www.codesdope.com/course/data-structures-binary-search-trees/)

[big O cheat sheet](https://www.bigocheatsheet.com/)

[Optimal memory reallocation and the golden ratio by Chris Taylor](https://archive.ph/Z2R8w)

# go

[How are Go channels implemented?](https://stackoverflow.com/questions/19621149/how-are-go-channels-implemented)

[Golang: Concurrency: Monitors and Mutexes, A (light) Survey](https://medium.com/dm03514-tech-blog/golang-monitors-and-mutexes-a-light-survey-84f04f9b7c09)

[Head first into sync.Cond of Golang. Pinku Deb Nath](https://medium.com/@pinkudebnath/head-first-into-sync-cond-of-golang-be71779699b1)

[The Case For A Go Worker Pool](https://www.brandur.org/go-worker-pool)

[Concurrency in Golang And WorkerPool [Part 1]](https://app.hackernoon.com/drafts/e5dIjipLStSzXtOn6McQ?ref=hackernoon.com)

[Goroutine Leaks - The Forgotten Sender](https://www.ardanlabs.com/blog/2018/11/goroutine-leaks-the-forgotten-sender.html)

[Leaking Goroutines](https://www.openmymind.net/Leaking-Goroutines/)

[Several Cases of Goroutine Leaks in Golang](https://www.sobyte.net/post/2021-06/several-cases-of-goroutine-leaks-in-golang/)

[Go vs Node vs Rust vs Swift](https://grigio.org/go-vs-node-vs-rust-vs-swift/)

[Dissecting Go Binaries](https://www.grant.pizza/dissecting-go-binaries/)

[awesome go](https://awesome-go.com/)

[Interfaces in Go by Uday Hiwarale](https://medium.com/rungo/interfaces-in-go-ab1601159b3a)

[How To Use Go Interfaces by Chewxy](https://blog.chewxy.com/2018/03/18/golang-interfaces/)

[Interface naming convention in Golang by Lucas Do](https://medium.com/@dotronglong/interface-naming-convention-in-golang-f53d9f471593)

[The Laws of Reflection by Rob Pike](https://blog.golang.org/laws-of-reflection)

[Errors are values by Rob Pike](https://blog.golang.org/errors-are-values)

[Defer, Panic, and Recover by Andrew Gerrand](https://blog.golang.org/defer-panic-and-recover)

[Error Handling in Go by Martin Kühl](https://www.innoq.com/en/blog/golang-errors-monads/)

[Arrays, slices (and strings): The mechanics of 'append' by Rob Pike](https://blog.golang.org/slices)

### federation, activity pub, webfinger .. standards

[Webfinger](https://datatracker.ietf.org/doc/html/rfc7033)

[How to implement remote following for your ActivityPub project](https://www.hughrundle.net/how-to-implement-remote-following-for-your-activitypub-project/)

[An ActivityPub server implementation example | Python](https://github.com/tOkeshu/activitypub-example)

[honk](https://humungus.tedunangst.com/r/honk)

[Blushy-Crushy Fediverse Idol: A Chat with Lain about Pleroma by Sean Tilley](https://medium.com/we-distribute/blushy-crushy-fediverse-idol-a-chat-with-lain-about-pleroma-4ff578b99752)

### architecture

[Structuring Applications in Go. Ben Johnson](https://medium.com/@benbjohnson/structuring-applications-in-go-3b04be4ff091)

[Standard Package Layout. Ben Johnson](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1)


### c [but not c++ ] 

[Understanding lvalues and rvalues in C and C++](https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c)

[C++ Rvalue References: The Unnecessarily Detailed Guide by betaveros](https://blog.vero.site/post/rvalue-references)

[Massacring C Pointers by Wozniak](https://wozniak.ca/blog/2018/06/25/1/index.html)

[Pointers Are Complicated, or: What's in a Byte? by ralfj.de](https://www.ralfj.de/blog/2018/07/24/pointers-and-bytes.html)

[Pointers Are Complicated II, or: We need better language specs by ralfj.de](https://www.ralfj.de/blog/2020/12/14/provenance.html)

[Alloca and Realloc – Useful Tools, Not Ancient Relics. blog.demofox.org](https://blog.demofox.org/2013/09/09/alloca-and-realloc-useful-tools-not-ancient-relics/)

### closure

[Closure conversion: How to compile lambda by matthew might](https://matt.might.net/articles/closure-conversion/)

### functional programming

[Subject: Re: FP, OO and relations. Does anyone trump the others?](https://okmij.org/ftp/Scheme/oop-in-fp.txt) [archive](https://web.archive.org/web/20081226055307/http://okmij.org/ftp/Scheme/oop-in-fp.txt)

### folklore

[Real Programmers Don’t Use Pascal by Ed Post](https://www.usm.uni-muenchen.de/~hoffmann/roff/tmp/rpdup.pdf)

[Real Programmers Don't Use Pascal](https://infogalactic.com/info/Real_Programmers_Don%27t_Use_Pascal)

[More About Real Programmers](https://web.archive.org/web/20080419225755/http://www.suslik.org/Humour/Computer/Langs/real_prog2.html)

[The Story of Mel, a Real Programmer by Ed Nather](http://www.pbm.com/~lindahl/mel.html)

### discussions

[Using Goto in Linux Kernel Code](https://koblents.com/Ches/Links/Month-Mar-2013/20-Using-Goto-in-Linux-Kernel-Code/) [lkml](https://lkml.org/lkml/2003/1/12/126)

[GPL vs BSD, a matter of sustainability (matusiak.eu). HN](https://news.ycombinator.com/item?id=5187235)

### memory

[Examining the Stack](https://web.archive.org/web/20210414194505/http://kirste.userpage.fu-berlin.de/chemnet/use/info/gdb/gdb_7.html)

[Function Call Stack Examples by Chris Kauffman](https://cs.gmu.edu/~kauffman/cs222/stack-demo.html)

[Where the top of the stack is on x86 by Eli Bendersky](https://eli.thegreenplace.net/2011/02/04/where-the-top-of-the-stack-is-on-x86/)

[Stack frames by Paul Krzyzanowski](https://people.cs.rutgers.edu/~pxk/419/notes/frames.html)

[Understanding the Stack](https://web.archive.org/web/20130225162302/http://www.cs.umd.edu/class/sum2003/cmsc311/Notes/Mips/stack.html)

[Spatial and Temporal Locality by Sergey Slotin](https://en.algorithmica.org/hpc/external-memory/locality/)

### call stack

[Writing a Linux Debugger Part 8: Stack unwinding by Sy Brand](https://blog.tartanllama.xyz/writing-a-linux-debugger-unwinding/)

[Deep Wizardry: Stack Unwinding by Josh Haberman](https://blog.reverberate.org/2013/05/deep-wizardry-stack-unwinding.html)

### tail call

[Tail Recursion. Lean](https://leanprover.github.io/functional_programming_in_lean/programs-proofs/tail-recursion.html)

[Tail Call Optimisation in C++ by Andy Balaam](https://accu.org/journals/overload/20/109/balaam_1914/)

[Performance of recursive functions (and when not to use them) by Guillaume Bogard](https://guillaumebogard.dev/posts/recursion-performance/)

[Tail Call Optimisation and C / C++ by Tristan Penman](https://tristanpenman.com/blog/posts/2018/12/08/tail-call-optimisation-and-c-cpp/)

[How Tail Call Optimization Works by Evan Klitzke](https://eklitzke.org/how-tail-call-optimization-works) [discussion. HN](https://news.ycombinator.com/item?id=25681362)

[The Story of Tail Call Optimizations in Rust by Sean Chen](https://dev.to/seanchen1991/the-story-of-tail-call-optimizations-in-rust-35hf)

[Why Object-Oriented Languages Need Tail Calls. reflection](http://www.eighty-twenty.org/2011/10/01/oo-tail-calls)

[How Tail Call Optimization Works by Evan Klitzke](https://eklitzke.org/how-tail-call-optimization-works)

[How Tail Call Optimization Works (eklitzke.org). HN](https://news.ycombinator.com/item?id=25681362)

[Procedures and the Processes They Generate. Interactive Structure and Interpretation of Computer Programs](https://xuanji.appspot.com/isicp/1-2-procedures.html) 
 
[Debunking the 'Expensive Procedure Call' Myth, or, Procedure Call Implementations Considered Harmful, or, Lambda: The Ultimate GOTO by Guy Steele](https://dspace.mit.edu/handle/1721.1/5753)

### assembly

[Stack and Functions](https://azeria-labs.com/functions-and-the-stack-part-7/)

### pinetime

[A look at Pine64: Pinetime by DarrowAtreides](https://itsmoss.com/2021/12/16/a-look-at-pine64-part-1-the-good/)

[PineTime: a smartwatch for open-source software by Sam Sloniker](https://lwn.net/Articles/941796/)

[Thoughts on my PineTime by Akseli Lahtinen](https://akselmo.dev/posts/thoughts-on-my-pinetime/)

### etc.

[HOW TO BUILD AN ECONOMIC MODEL IN YOUR SPARE TIME](https://people.ischool.berkeley.edu/~hal/Papers/how.pdf)
