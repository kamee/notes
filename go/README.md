### typesystem

[A Closer Look at Go (golang) Type System by Ankur Anand](https://blog.ankuranand.com/2018/11/29/a-closer-look-at-go-golang-type-system/)

### http / networking

[IV. Adding Context to Requests by fideloper](https://fideloper.com/golang-context-http-middleware)

### addressabilty

[There is no pass-by-reference in Go](https://dave.cheney.net/2017/04/29/there-is-no-pass-by-reference-in-go)

[If a map isn’t a reference variable, what is it?](https://dave.cheney.net/2017/04/30/if-a-map-isnt-a-reference-variable-what-is-it)

[Addressable values in Go (and unaddressable ones too)](https://utcc.utoronto.ca/~cks/space/blog/programming/GoAddressableValues) 

### container types: slices, maps, arrays

[SliceTricks by Duncan Harris](https://github.com/golang/go/wiki/SliceTricks)

[Proposal: Multi-dimensional slices by Brendan Tracey](https://go.googlesource.com/proposal/+/master/design/6282-table-data.md)

[Arrays, slices (and strings): The mechanics of 'append' by Rob Pike](https://go.dev/blog/slices)

[How can a slice contain itself?](https://stackoverflow.com/questions/36077566/how-can-a-slice-contain-itself/36078970#36078970)

[If a map isn’t a reference variable, what is it?](https://dave.cheney.net/2017/04/30/if-a-map-isnt-a-reference-variable-what-is-it)

[Arrays, Slices and Maps in Go](https://go101.org/article/container.html)

[Macro View of Map Internals In Go](https://www.ardanlabs.com/blog/2013/12/macro-view-of-map-internals-in-go.html)

[How the Go runtime implements maps efficiently (without generics) by Dave Cheney](https://dave.cheney.net/2018/05/29/how-the-go-runtime-implements-maps-efficiently-without-generics)

### pointers

[Type-Unsafe Pointers](https://go101.org/article/unsafe.html)

### errors

[Elegance of Go's error handling by Ville Hakulinen](https://thingsthatkeepmeupatnight.dev/posts/golang-http-handler-errors/)

### architecture

[How Do You Structure Your Go Apps by Kat Zien (gopher con talk)](https://youtu.be/oL6JBUk6tj0)

***notes:*** ddd, production code

[Best Practices for Industrial Programming by Peter Bourgon (gopher con talk)](https://youtu.be/PTE4VJIdHPg)

### concurrency

[[Golang] Channel by dev-yakuza](https://dev-yakuza.posstree.com/en/golang/channel/)

[Go channels on steroids by Dmitry Vyukov](https://docs.google.com/document/d/1yIAYmbvL3JxOKOjuCyon7JhW4cSv1wy5hC0ApeGMV9s/pub)

[The new kid in town — Go’s sync.Map by Ralph Caraveo](https://medium.com/@deckarep/the-new-kid-in-town-gos-sync-map-de24a6bf7c2c)

[Goroutine leak detector](https://github.com/uber-go/goleak)

[Diving Deep Into The Golang Channels by Ankur Anand](https://codeburst.io/diving-deep-into-the-golang-channels-549fd4ed21a8)

[Internals of Go Channels by Shubham Agrawal](https://shubhagr.medium.com/internals-of-go-channels-cf5eb15858fc)

[How did I improve latency by 700% using sync.Pool by Akshay Deo](https://www.akshaydeo.com/)

[Never start a goroutine without knowing how it will stop by Dave Cheney](https://dave.cheney.net/2016/12/22/never-start-a-goroutine-without-knowing-how-it-will-stop)

[Reference Counting in Go by Eric's Apparatus](http://www.hydrogen18.com/blog/reference-counted-pool-golang.html)

[Go 1.5 GOMAXPROCS Default (discussion on golang-dev) Russ Cox](https://docs.google.com/document/d/1At2Ls5_fhJQ59kDK2DFVhFu3g5mATSXqqV5QrxinasI/preview#)

### opinions

[Why I’m not leaving Python for Go by Yuval Greenfield](https://uberpython.wordpress.com/2012/09/23/why-im-not-leaving-python-for-go/)

### resources

[Go FAQ 101](https://go101.org/article/unofficial-faq.html)
