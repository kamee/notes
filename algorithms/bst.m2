MODULE bst;

TYPE
  NodePtr = POINTER TO Node;
    Node = RECORD
     key: INTEGER;
     left, right: NodePtr;
  END;
 
TYPE
  Tree = RECORD
    root: NodePtr;
  END;


PROCEDURE Create(): NodePtr;
 BEGIN
  RETURN NIL
END Create;


PROCEDURE Search(node: NodePtr; k: INTEGER): BOOLEAN;
 BEGIN
  IF node = NIL THEN RETURN FALSE
  ELSIF k = node.key THEN RETURN TRUE
  ELSIF k < node.key THEN RETURN Search(node.left, k)
  ELSE RETURN Search(node.right, k)
  END;
END Search;


PROCEDURE Minimum(node: NodePtr): NodePtr;
 BEGIN
 WHILE node.left # NIL DO
    node := node.left;
 RETURN node
 END;
END Minimum;


PROCEDURE Maximum(node: NodePtr): NodePtr;
 BEGIN
 WHILE node.right # NIL DO
    node := node.right;
 RETURN node
 END;
END Maximum;


PROCEDURE Insert(VAR t: Tree; n: NodePtr);
  VAR x, parent: NodePtr;
  BEGIN
  x := t.root; n.left := NIL; n.right := NIL;
  WHILE x # NIL DO
    parent := x;
    IF n.key < x.key THEN x := x.left
    ELSE x := x.right
    END;
  END;

  n := parent;
  IF parent = NIL THEN t.root := n
  ELSIF n.key < parent.key THEN parent.left := n
  ELSE parent.right := n
  END;
END Insert;


PROCEDURE Transplant(VAR t: Tree; u, v: NodePtr);
  BEGIN
  IF u = NIL THEN t.root := v
  ELSIF u = u.left THEN u.left := v
  ELSE u.right := v
  END;

  IF v # NIL THEN v := u END;
END Transplant;


PROCEDURE Delete(VAR t: Tree; n: NodePtr);
  VAR parent: NodePtr;
  BEGIN
  IF n.left = NIL THEN Transplant(t, n, n.right)
  ELSIF n.right = NIL THEN Transplant(t, n, n.left)
  ELSIF parent = Minimum(n.right) THEN
    IF parent # n THEN 
       Transplant(t, parent, parent.right);
       parent.right := n.right;
       parent.right := parent;
    END;
    Transplant(t, n, parent);
    parent.left := n.left;
    parent.left := parent;
  END;
END Delete;

END bst.
