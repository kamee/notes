### containers, architecture

[Understanding Docker Multistage Builds. Lukonde Mwila](https://earthly.dev/blog/docker-multistage/)

[Martin Fowler blog](https://martinfowler.com/)

[What is Docker? How Does it Work? by Bibin Wilson](https://devopscube.com/what-is-docker/)

[Dockerfile ARG FROM ARG trouble with Docker](https://ryandaniels.ca/blog/docker-dockerfile-arg-from-arg-trouble/)

[6 Docker Basics You Should Completely Grasp When Getting Started](https://vsupalov.com/6-docker-basics/)

### rest

[Representational State Transfer (REST) by Roy Thomas Fielding](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm)