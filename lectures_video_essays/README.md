### ճարտարապետություն

["Push the city" | «Քաղաք․Փուշ»](https://youtu.be/Wi6iVOx23Dk?si=61KnrUYcXivclM21)

### քաղաքական

[Պատմության ընկալումը և քաղաքական մարտահրավերները. Ժիրայր Լիպարիտյան](https://www.youtube.com/watch?v=93uIIv67hFQ)

[PS ռետրո Հյուրը Ժիրայր Լիպարիտյանն է](https://www.youtube.com/watch?v=3KbjQL43Xdk)

[ԼՏՊ բանակցություններ](https://www.youtube.com/watch?v=6t2PAfRsL3A)

### փիլիսոփայություն

[A Conversation on Kierkegaard's "The Present Age"| Overthink Podcast](https://youtu.be/KvCTxGNYyUA?si=uEEdJ1W9i6pQ5vSc)

[Hegelian Recognition and Incels](https://youtu.be/n1flcGrb81M?si=fK1LCRxdMWOHfBUw)

[Intro to Hegel (& Progressive Politics) | Philosophy Tube](https://youtu.be/OgNt1C72B_4?si=5fneVZiOj2WPI_wr)

[Capitalism is dead. This is Technofeudalism. (Yanis Varoufakis) | Philosophize This!](https://youtu.be/mZo3PRxbdUw?si=nUdMYJ0Pp0F0haDE)

[Debt | Overthink Podcast](https://youtu.be/aw9E49bdfXE?si=d12X0DY0DGCOKqJu)

[Intensity | Overthink Podcast](https://youtu.be/uxzX0ENgKxw?si=pZKIJr7iTNWEpqw-)

[Վարդան Ազատյան, ««Ո՞վ է այս Վինքելմանը»․ գեղագիտական անտիկա և արդիական հայեր», մաս I](https://youtu.be/yBAJo3VTEd4?si=i-hsryeFCPDFp8V6)

[Վարդան Ազատյան, ««Ո՞վ է այս Վինքելմանը»․ գեղագիտական անտիկա և արդիական հայեր», մաս II](https://youtu.be/7ugI1ubj7Hc?si=ugeU4KemcN6uSA0N)

[Վարդան Ազատյան «Ծնողազուրկ ծննդից հետո»․ որբություն, քաղաքականություն, արվեստ](https://youtu.be/HOdKRd0JYT0?si=Iyo2EpnEt8MiwGAC)

[Envy | ContraPoints](https://youtu.be/aPhrTOg1RUk?si=d_mrlzdXxRWpOlQK)

[Jordan Peterson doesn't understand postmodernism | Jonas Čeika - CCK Philosophy](https://youtu.be/cU1LhcEh8Ms?si=tGepWtALPkS4aNwm)

[The Simpsons and the Death of Parody | Jonas Čeika - CCK Philosophy](https://youtu.be/hi_fxwLBSFo?si=_oDzBrTdpJD7SXIV)

[Philipp Mainländer: The Life-Rejecting Socialist | Jonas Čeika - CCK](https://youtu.be/nnJmA9_dkP0?si=eVWW887LXczSkTPK)

[Վարդան Ազատյան. հանրային ոլորտ](https://youtu.be/2KG2deZOpiA?si=IpdxXxU2c4ozPCeV)

[Վարդան Ազատյան.ազգայինը և հակասությունները](https://youtu.be/hspv39ROM98?si=LBbVJ5OnCBDgkSne)

[＜free culture＞ Lawrence Lessig](https://youtu.be/xlLnK4ugTLc?si=Xygz_ZNSGefUMlyp)

[Արվեստ և ժամանակակից արվեստ.Վարդան Ազատյան](https://youtu.be/pScIkUEtQBw?si=rohJbOztxg4TOtsX)

[Վարդան Ազատյան. Հայ քաղաքական ուտոպիաները](https://youtu.be/Hq_S267pC90?si=9iBjHV1dcLfU-H4R)

[Վարդան Ազատյան. Խորհրդային Հայաստանի 1960-70-ականները](https://youtu.be/78s_ro-lfX8?si=94iJRlq9SjGlJdCj)

[Հայ իրականության Տեսիլի մահից անդին](https://youtu.be/4ppwQhRTtW4?si=N6v55wm6t3dK996S)

[Մարտահրավերը | Վարդան Ազատյան](https://youtu.be/X-4xMHgWJ1o?si=ZffjoqO0aj1GsZVO)



### etc.

[rumination | combatting repetitive thoughts [cc] | TheraminTrees](https://youtu.be/o1G4JFuLlO8?si=DNbpcTKTUlGPbvT0)

### irony

[We need to talk about irony.](https://youtu.be/_ZNCCttVMg8?si=L5z9J2udhm-gY82d)

[Post-Irony, Meta-Irony, and Post-Truth Satire](https://youtu.be/nsuSveDAlpI?si=pAQsB3xpfY1OFFJG)
