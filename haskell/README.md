### general

[Happy User Guide by Simon Marlow, Andy Gill](https://monlih.github.io/happy-docs/)

[To the Budget Council (concerning Haskell) by Edsger Dijikstra](https://www.cs.utexas.edu/~EWD/transcriptions/OtherDocs/Haskell.html)

[Haskell2020 Is Dead, but All Hope Is Not Lost](https://reasonablypolymorphic.com/blog/haskell202x/)

[Why Haskell matters](https://wiki.haskell.org/Why_Haskell_matters)

[Data is Code](https://www.haskellforall.com/2016/04/data-is-code.html)

[Why Haskell Matters](https://github.com/thma/WhyHaskellMatters)

[Why Functional Programming Matters by John Hughes](https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf)

### functions

[Haskell Functions by Carl Burch](http://www.cburch.com/books/hsfun/)

[dollar sign](https://typeclasses.com/featured/dollar)

[Category: The Essence of Composition by Bartosz Milewski](https://bartoszmilewski.com/2014/11/04/category-the-essence-of-composition/)

### partial application

[Avoiding partial functions. wiki haskell](https://wiki.haskell.org/Avoiding_partial_functions)

### projects, tools, resources

[A lambda calculus interpreter in Haskell by Jamesh Fisher](https://jameshfisher.com/2018/03/15/a-lambda-calculus-interpreter-in-haskell/)

[Haskell-Euterpea-SevenNationArmy-tutorial. MaciejWanat](https://github.com/MaciejWanat/Haskell-Euterpea-SevenNationArmy-tutorial/tree/master)

[The nhc98 compiler](https://www.haskell.org/nhc98/)

[Hugs 98](https://www.haskell.org/hugs/)

[The Haskell Tool Stack](https://docs.haskellstack.org/en/stable/)

### closure

[RE: What's so cool about Scheme?](http://people.csail.mit.edu/gregs/ll1-discuss-archive-html/msg03277.html)

### papers, opinion

[A History of Haskell: Being Lazy With Class by Paul Hudak](https://web.archive.org/web/20160321231319/http://haskell.cs.yale.edu/wp-content/uploads/2011/02/history.pdf)

### functional programming

[Functional Programming with Bananas, Lenses,Envelopes and Barbed Wire by Erik Meijer, Maarten Fokkinga, Ross Paterson](https://maartenfokkinga.github.io/utwente/mmf91m.pdf)

[Type Classes with Functional Dependencies by Mark P. Jones](http://web.cecs.pdx.edu/~mpj/pubs/fundeps-esop2000.pdf)

[It’s All About Morphisms by Bartosz Milewski](https://bartoszmilewski.com/2015/11/17/its-all-about-morphisms/)

### functional programming books, resources

R. Bird and P. Wadler. Introduction to Functional Programming

### haskell

[Into the Core - Squeezing Haskell into Nine Constructors by Simon Peyton Jones](https://youtu.be/uR_VzYxvbxg)

[Typing Haskell in Haskell by MARK P. JONES](https://web.cecs.pdx.edu/~mpj/thih/thih.pdf)

### monads

[Understanding Haskell Monads by Ertugrul Söylemez](https://web.archive.org/web/20120114225257/http://ertes.de/articles/monads.html)

[All about monads](https://wiki.haskell.org/All_About_Monads) [paper](https://www.cs.rit.edu/~swm/cs561/All_About_Monads.pdf)

[Monads for functional programming by Philip Wadler](https://homepages.inf.ed.ac.uk/wadler/papers/marktoberdorf/baastad.pdf)

[Monadic parsing in Haskell by Graham Hutton](https://www.cs.tufts.edu/comp/150FP/archive/graham-hutton/monadic-parsing-jfp.pdf)

[Monads for functional programming by Philip Wadler](https://homepages.inf.ed.ac.uk/wadler/papers/marktoberdorf/baastad.pdf)

### functor

[Representable Functors by Bartosz Milewski](https://bartoszmilewski.com/2015/07/29/representable-functors/)

[Functors are Containers by Bartosz Milewski](https://bartoszmilewski.com/2014/01/14/functors-are-containers/)

[Monads Categorically by Bartosz Milewski](https://bartoszmilewski.com/2016/12/27/monads-categorically/)

[George Wilson - The Extended Functor Family](https://www.youtube.com/watch?v=JZPXzJ5tp9w)

### haskell concurrency

[Composable Memory Transactions by Tim Harris, Simon Marlow, Simon Peyton Jones, Maurice Herlihy](https://www.microsoft.com/en-us/research/wp-content/uploads/2005/01/2005-ppopp-composable.pdf?from=https%3A%2F%2Fresearch.microsoft.com%2Fen-us%2F)

### haskell resources, books

[Haskell 2023: Proceedings of the 16th ACM SIGPLAN International Haskell Symposium](https://www.sigplan.org/OpenTOC/haskell23.html)

The Haskell School of Expression. Learning Functional Programming through Multimedia by Paul Hudak

Partial Evaluation and Automatic Program Generation by Neil D. Jones, Carsten K. Gomard, Peter Sestoft

https://github.com/krispo/awesome-haskell

### projects

Let’s Build a Compiler (in Haskell)
https://g-ford.github.io/cradle/

https://github.com/veniversum/fractal-haskell

### podcasts

https://haskell.foundation/

https://haskellweekly.news/podcast.html
