###heapsort

#basic heap structure
root of a tree: first element(i = 1)
parent(i) = i/2
left(i) = 2i, right(i) = 2i+1

#max/min heap property
the key of a node is >=(<=) the keys of its children

A[PARENT(i)] >= A[i] or A[PARENT(i)] <= A[i]

elements A[n/2 + 1...n] are all leaves

PARENT(i)
1. return[i/2]

LEFT(i)
1. return 2i

RIGHT(i)
1. return 2i+1

# PSEUDO

Max-HEAPIFY(A.i)
>---l = LEFT(i)
>---r = RIGHT(i)

>---if l <= A.heap-size and A[l] > A[i]:
>--->---largest = l
>---else largest = i

	if r <= A.heap-size and A[r] > A[largest]
>--->---largest = r
>---if largest # i
>--->---exchange A[i] with A[largest]
>--->---MAX-HEAPIFY(A.largest)


### Binary tree

THEOREM. If x is the root of an n-node subtree, then the call INORDER-TREE-WALK(x) takes O(n) time.

Run-time = O(h)
