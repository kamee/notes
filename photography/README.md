### blogposts

[Optimizing Your Night Street Photography by Lukasz Palka](https://www.eyexplore.com/blog/optimizing-your-night-street-photography/)

### darktable

[Luminosity Masks in darktable by mueen](https://www.darktable.org/2015/01/luminosity-masks-in-darktable/)

### exposure

[Overexposed vs Underexposed – Which Is Better for Your Photos? by Kevin Landwer-Johan](https://expertphotography.com/underexposure-vs-overexposure/)

[CAMERA EXPOSURE by cambridgeincolour.com](https://www.cambridgeincolour.com/tutorials/camera-exposure.htm)

[Exposure vs. Brightening by gollywop](https://www.dpreview.com/articles/8148042898/exposure-vs-brightening)

[What is Exposure Compensation and How to Use It by Nasim Mansurov](https://photographylife.com/what-is-exposure-compensation)

[The Ultimate Beginner's Introduction to Exposure by Peter Tellone](https://photography.tutsplus.com/tutorials/the-ultimate-beginners-introduction-to-exposure--photo-3028)

[Learning Multi-Scale Photo Exposure Correction by Mahmoud Afifi, Konstantinos G. Derpanis, Bj¨orn Ommer, Michael S. Brown](https://openaccess.thecvf.com/content/CVPR2021/papers/Afifi_Learning_Multi-Scale_Photo_Exposure_Correction_CVPR_2021_paper.pdf)


### colour theory

[Colour models](http://learn.colorotate.org/color-models/)

[HSL](https://web.cs.uni-paderborn.de/cgvb/colormaster/web/color-systems/hsl.html)

[A Perception-based Color Space for Illumination-invariant
Image Processing by Hamilton Y. Chong, Steven J. Gortler, Todd Zickler](http://www.cs.harvard.edu/~sjg/papers/cspace.pdf)

### colour balance

[Color Balance in Digital Imaging](https://www.microscopyu.com/digital-imaging/color-balance-in-digital-imaging)

### composition

[Filmmaking 101: What Is the Rule of Thirds & How Filmmakers Use It (and break it) by Daniela Bowker](https://artlist.io/blog/rule-of-thirds/)
