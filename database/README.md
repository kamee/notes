### database

### database indexes

[Tutorial on MySQL Database Optimization using Indexes by Benson Kariuki](https://www.section.io/engineering-education/mysql-query-optimization-using-indexes-with-examples/)

[Indexing. by  Blake Barnhill](https://dataschool.com/sql-optimization/how-indexing-works/)

[How do database indexes work?. Justin Gage](https://planetscale.com/blog/how-do-database-indexes-work#footnotes)

[Introduction to Indexes by Gail Shaw](https://www.sqlservercentral.com/articles/introduction-to-indexes)

**notes: clustered, nonclustered, index limits, scans, seeks, lookups, updates**

[Index Filter Predicates Used Intentionally by Markus Winand](https://use-the-index-luke.com/sql/clustering/index-filter-predicates)

**note: index range scan, choose index**

### design

[Database Normalization in SQL with Examples by gourishankar](https://www.sqlservercentral.com/articles/database-normalization-in-sql-with-examples)

[Database Normalization By: Kris Wenzel](https://www.essentialsql.com/database-normalization/)

### relationships

[Database Modeling - Relationships by Ash](https://www.sqlservercentral.com/articles/database-modeling-relationships)

### optimization

[Optimization with EXPLAIN ANALYZE](https://dataschool.com/sql-optimization/optimization-using-explain/)

[Explain Explained by Josh Berkus](https://youtu.be/mCwwFAl1pBU)

[Introduction to VACUUM, ANALYZE, EXPLAIN, and COUNT](https://wiki.postgresql.org/wiki/Introduction_to_VACUUM,_ANALYZE,_EXPLAIN,_and_COUNT)

### offset

[We need tool support for keyset pagination by Markus Winand](https://use-the-index-luke.com/no-offset)

[Understanding Offset vs Cursor based pagination by Rahul](https://dev.to/rahul_ramfort/understanding-offset-vs-cursor-based-pagination-1582)

### partitioning

[Postgres Partitioning by Sean Hellebusch](https://www.hingehealth.com/engineering-blog/postgres-partitioning/)

### resources

[The Data School Library](https://dataschool.com/books/)

